// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::any::Any;

use crate::{
    errors::CompileError, syntax::keywords::KEYWORDS, tokenizer::TokenType, utils::Position,
};

use super::{CaptureGroup, ParseContext, ParseNode, ValueNode};

/// Matches parse node and captures data with capture group.
#[derive(Debug, Clone, Copy)]
pub struct Group<T, F, R>
where
    T: ParseNode,
    F: Fn(CaptureGroup) -> R,
{
    pub grammar: T,
    pub callback: F,
}

impl<T, F, R> ValueNode for Group<T, F, R>
where
    T: ParseNode,
    F: Fn(CaptureGroup) -> R,
    R: Any,
{
    type ValueType = R;

    fn _parse(&self, ctx: &mut ParseContext) -> Option<Result<Self::ValueType, CompileError>> {
        let mut group = CaptureGroup::new();
        match self.grammar.try_parse(ctx, &mut group) {
            Some(Ok(())) => Some(Ok((self.callback)(group))),
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

/// Matches value and saves result.
impl<T, V> ParseNode for (&'static str, T)
where
    T: ValueNode<ValueType = V>,
    V: 'static,
{
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let (key, value_node) = self;
        match value_node.parse_to_val(ctx) {
            Some(Err(err)) => Some(Err(err)),
            Some(Ok(val)) => {
                group.set(key, val);
                Some(Ok(()))
            }
            None => None,
        }
    }
}

/// Matches group and pushes result to vector.
#[derive(Debug, Clone, Copy)]
pub struct Repetition<T, V>(pub &'static str, pub T)
where
    T: ValueNode<ValueType = V>,
    V: 'static;

impl<T, V> ParseNode for Repetition<T, V>
where
    T: ValueNode<ValueType = V>,
    V: 'static + Clone,
{
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let Self(key, value_node) = self;
        match value_node.parse_to_val(ctx) {
            Some(Err(err)) => Some(Err(err)),
            Some(Ok(val)) => {
                group.push(key, val);
                Some(Ok(()))
            }
            None => None,
        }
    }
}

/// Matches its child.
///
/// Used to simplify node type in some AST nodes.
pub struct Node(pub Box<dyn ParseNode>);

impl ParseNode for Node {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        self.0.try_parse(ctx, group)
    }
}

/// Matches any of given nodes.
pub struct AnyOf(pub Vec<Box<dyn ParseNode>>);

impl ParseNode for AnyOf {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let Self(children) = self;
        for child in children.iter() {
            if let Some(res) = child.try_parse(ctx, group) {
                return Some(res);
            }
        }
        None
    }
}

/// Matches sequence of given nodes.
pub struct Sequence(pub Vec<Box<dyn ParseNode>>);

impl ParseNode for Sequence {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let Self(children) = self;
        for child in children.iter() {
            if let Err(err) = child.try_parse(ctx, group)? {
                return Some(Err(err));
            }
        }

        Some(Ok(()))
    }
}

/// Fails with given error message
pub struct Error(pub &'static str);

impl ParseNode for Error {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        _: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let Self(message) = self;
        Some(Err(ctx.error(message)))
    }
}

/// Matches its child and fails on success.
pub struct Fail<T: ParseNode>(pub T);

impl<T: ParseNode> ParseNode for Fail<T> {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let Self(child) = self;

        match child.try_parse(ctx, group) {
            Some(Err(err)) => Some(Err(err)),
            Some(Ok(_)) => None,
            None => Some(Ok(())),
        }
    }
}

/// Matches child zero or more times.
pub struct ZeroOrMore<T: ParseNode>(pub T);

impl<T: ParseNode> ParseNode for ZeroOrMore<T> {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let Self(child) = self;
        loop {
            match child.try_parse(ctx, group) {
                Some(Ok(_)) => (),
                Some(Err(err)) => return Some(Err(err)),
                None => break,
            }
        }

        Some(Ok(()))
    }
}

/// Matches child delimited by delimiter zero or more times.
pub struct Delimited<C: ParseNode, D: ParseNode>(pub C, pub D);

impl<C: ParseNode, D: ParseNode> ParseNode for Delimited<C, D> {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let Self(child, delimeter) = self;
        match child.try_parse(ctx, group) {
            Some(Err(err)) => return Some(Err(err)),
            None => return Some(Ok(())),
            _ => (),
        }

        loop {
            ctx.start_parsing();
            match delimeter.try_parse(ctx, group) {
                Some(Err(err)) => {
                    ctx.fail_parsing();
                    return Some(Err(err));
                }
                None => {
                    ctx.fail_parsing();
                    break;
                }
                _ => (),
            }

            match child.try_parse(ctx, group) {
                Some(Err(err)) => {
                    ctx.fail_parsing();
                    return Some(Err(err));
                }
                None => {
                    ctx.fail_parsing();
                    break;
                }
                _ => (),
            }

            ctx.finish_parsing();
        }

        Some(Ok(()))
    }
}

/// Matches child until terminator is matched.
pub struct Until<C: ParseNode, T: ParseNode>(pub C, pub T);

impl<C: ParseNode, T: ParseNode> ParseNode for Until<C, T> {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let Self(child, terminator) = self;
        loop {
            if let Some(res) = terminator.try_parse(ctx, group) {
                return Some(res);
            }

            match child.try_parse(ctx, group) {
                Some(Ok(())) => (),
                err => return err,
            }
        }
    }
}

/// Matches child zero or one times.
pub struct Optional<T: ParseNode>(pub T);

impl<T: ParseNode> ParseNode for Optional<T> {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let Self(child) = self;
        match child.try_parse(ctx, group) {
            Some(Err(err)) => Some(Err(err)),
            _ => Some(Ok(())),
        }
    }
}

/// Matches token of given string.
impl ParseNode for &'static str {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        _: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let token = ctx.next()?;

        if token.as_str() == *self {
            Some(Ok(()))
        } else {
            None
        }
    }
}

/// Matches its child and returns its position.
pub struct WithPosition<C: ParseNode>(pub &'static str, pub C);

impl<C: ParseNode> ParseNode for WithPosition<C> {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let Self(key, child) = self;
        let start = ctx.start_pos();
        match child.try_parse(ctx, group) {
            Some(Ok(())) => {
                let end = ctx.end_pos();
                group.set(
                    key,
                    Position {
                        source: ctx.source().clone(),
                        start,
                        end,
                    },
                );
                Some(Ok(()))
            }
            other_res => other_res,
        }
    }
}

/// Matches valid identifier.
pub struct Ident(pub &'static str);

impl ParseNode for Ident {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let &Self(key) = self;
        let token = ctx.next()?;

        if token.token_type == TokenType::Ident && !KEYWORDS.contains(&token.as_str()) {
            group.set(key, token.as_str().to_string());
            Some(Ok(()))
        } else {
            None
        }
    }
}

/// Matches integer.
pub struct Integer(pub &'static str);

impl ParseNode for Integer {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let &Self(key) = self;
        let token = ctx.next()?;

        if token.token_type == TokenType::Integer {
            group.set(
                key,
                match token.as_str().parse::<i64>() {
                    Ok(num) => num,
                    Err(_) => return Some(Err(ctx.error("Failed to parse decimal number"))),
                },
            );
            Some(Ok(()))
        } else {
            None
        }
    }
}

/// Matches integer and saves it as text.
pub struct IntegerText(pub &'static str);

impl ParseNode for IntegerText {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        Group {
            grammar: Integer("val"),
            callback: |group| group.get::<i64>("val").unwrap().to_string(),
        }
        .try_parse(ctx, group)
    }
}

/// Matches string literal.
pub struct Quoted(pub &'static str);

impl ParseNode for Quoted {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let &Self(key) = self;
        let token = ctx.next()?;

        if token.token_type == TokenType::Quoted {
            let token_value = token.as_str();
            group.set(key, token_value[1..token_value.len() - 1].to_string());
            Some(Ok(()))
        } else {
            None
        }
    }
}

/// Matches nothing and sets given value to given slot.
pub struct SetValue<T: Any + Clone>(pub &'static str, pub T);

impl<T: Any + Clone> ParseNode for SetValue<T> {
    fn _parse(
        &self,
        _: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        let Self(key, val) = self;
        group.set(key, val.clone());
        Some(Ok(()))
    }
}

/// Eof
pub struct Eof;

impl ParseNode for Eof {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        _: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        if ctx.next()?.token_type == TokenType::Eof {
            Some(Ok(()))
        } else {
            None
        }
    }
}
