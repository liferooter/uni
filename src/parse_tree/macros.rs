// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#[macro_export]
macro_rules! sequence {
    ($first:expr $(, $arg:expr)*) => {
        $crate::parse_tree::Sequence(vec![
            Box::new($first)
            $(, Box::new($arg))*
        ])
    };
}

#[macro_export]
macro_rules! any_of {
    ($first:expr $(, $arg:expr)*) => {
        $crate::parse_tree::AnyOf(vec![
            Box::new($first)
            $(, Box::new($arg))*
        ])
    };
}

#[macro_export]
macro_rules! node {
    ($arg:expr) => {
        $crate::parse_tree::Node(Box::new($arg))
    };
}
