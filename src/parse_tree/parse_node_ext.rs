// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::parse_tree::{any_of, node, Error, Node};

use super::ParseNode;

/// Trait that contains additional methods for `ParseNode`
pub trait ParseNodeExt {
    fn or_err(self, error: &'static str) -> Node;
    fn required(self) -> Node;
}

impl<T: 'static + ParseNode> ParseNodeExt for T {
    fn or_err(self, error: &'static str) -> Node {
        node!(any_of!(self, Error(error)))
    }

    fn required(self) -> Node {
        node!(any_of!(self, Error("syntax error")))
    }
}
