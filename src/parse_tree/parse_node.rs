// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{any::Any, collections::HashMap};

use crate::errors::CompileError;

use super::parse_ctx::ParseContext;

type GroupState = HashMap<String, Box<dyn Any>>;

/// Capture group that captures values in slots.
#[derive(Debug)]
pub struct CaptureGroup {
    states: Vec<GroupState>,
}

impl CaptureGroup {
    /// Create new capture group.
    pub fn new() -> Self {
        Self {
            states: vec![HashMap::new()],
        }
    }

    /// Sets given value to given slot of capture group.
    pub fn set<T: Any>(&mut self, key: &str, val: T) {
        self.states
            .last_mut()
            .expect("Can't insert value when no parsing started")
            .insert(key.to_string(), Box::new(val));
    }

    /// Get value from given slot.
    pub fn get<T: Any>(&self, key: &str) -> Option<&T> {
        self.states
            .iter()
            .rfind(|state| state.contains_key(key))
            .and_then(|state| state.get(key))
            .and_then(|val| val.downcast_ref())
    }

    /// Take value from given slot.
    pub fn take<T: Any>(&mut self, key: &str) -> Option<T> {
        self.states
            .iter_mut()
            .rfind(|state| state.contains_key(key))
            .and_then(|state| state.remove(key))
            .and_then(|val| val.downcast().ok())
            .map(|val| *val)
    }

    /// Append value to given slot.
    pub fn push<T: Any + Clone>(&mut self, key: &str, val: T) {
        let mut array = self
            .states
            .iter_mut()
            .rfind(|state| state.contains_key(key))
            .and_then(|state| state.get(key))
            .and_then(|elem| elem.downcast_ref::<Vec<T>>())
            .cloned()
            .unwrap_or_default();
        array.push(val);
        self.set(key, array);
    }

    /// Create new state when sub-node parsing started.
    pub fn start_parsing(&mut self) {
        self.states.push(HashMap::new())
    }

    /// Stage changes when sub-node parsing is finished.
    pub fn finish_parsing(&mut self) {
        let changes = self
            .states
            .pop()
            .expect("Can't finish parsing when no parsing started");
        let state = self.states.last_mut().unwrap();

        for (key, val) in changes.into_iter() {
            state.insert(key, val);
        }
    }

    /// Undo changes when sub-node parsing is failed.
    pub fn fail_parsing(&mut self) {
        self.states.pop();
    }
}

/// Parse node object.
///
/// Tries to parse itself from parse context.
pub trait ParseNode {
    /// Try to parse node.
    ///
    /// Returns `None` if failed to parse, parsed node
    /// if succeed and compile error if an error occured.
    fn try_parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        ctx.start_parsing();
        group.start_parsing();
        let res = self._parse(ctx, group);
        match res {
            Some(Ok(_)) => {
                group.finish_parsing();
                ctx.finish_parsing();
            }
            _ => {
                group.fail_parsing();
                ctx.fail_parsing();
            }
        }
        res
    }

    /// Internal implementation of node parsing.
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        group: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>>;
}

/// Parse node that produces the value.
pub trait ValueNode {
    type ValueType;
    /// Parse node and get value.
    fn parse_to_val(
        &self,
        ctx: &mut ParseContext,
    ) -> Option<Result<Self::ValueType, CompileError>> {
        ctx.start_parsing();
        let res = self._parse(ctx);
        match res {
            Some(Ok(_)) => ctx.finish_parsing(),
            _ => ctx.fail_parsing(),
        }
        res
    }

    /// Internal implementation of value parsing.
    fn _parse(&self, ctx: &mut ParseContext) -> Option<Result<Self::ValueType, CompileError>>;
}

impl<T: ValueNode> ParseNode for T {
    fn _parse(
        &self,
        ctx: &mut ParseContext,
        _: &mut CaptureGroup,
    ) -> Option<Result<(), CompileError>> {
        match self.parse_to_val(ctx) {
            Some(Ok(_)) => Some(Ok(())),
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}
