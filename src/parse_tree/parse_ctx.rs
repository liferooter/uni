// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::{
    errors::CompileError,
    runtime::CodeSource,
    tokenizer::{tokenize, Token},
    utils::Position,
};

use super::*;

/// Parse context.
pub struct ParseContext {
    tokens: Vec<Token>,
    pos: usize,
    fail_pos: Option<usize>,
    saved_positions: Vec<usize>,
}

impl ParseContext {
    pub fn new(source: &CodeSource, code: &str) -> Result<Self, CompileError> {
        Ok(Self {
            tokens: tokenize(source, code)?,
            pos: 0,
            fail_pos: Some(0),
            saved_positions: Vec::new(),
        })
    }

    /// Save state of iterator before parsing
    pub fn start_parsing(&mut self) {
        self.saved_positions.push(self.pos);
    }

    /// Drop saved state when the parsing is finished
    pub fn finish_parsing(&mut self) {
        self.fail_pos = None;
        self.saved_positions
            .pop()
            .expect("Can't finish parsing when no parsing started");
    }

    /// Restore saved state when parsing is failed
    pub fn fail_parsing(&mut self) {
        if self.fail_pos.is_none() {
            self.fail_pos = Some(self.pos);
        }
        self.pos = self
            .saved_positions
            .pop()
            .expect("Can't fail parsing when no parsing started");
    }

    /// Generate error with given message
    pub fn error(&self, message: &str) -> CompileError {
        CompileError::new(message, &self.tokens[self.pos].position)
    }

    /// Parse whole file as given node
    pub fn parse<T: ValueNode + 'static>(&mut self, node: T) -> Result<T::ValueType, CompileError> {
        let mut group = CaptureGroup::new();
        let res = sequence!(("value", node), any_of!(Eof, Error("unexpected tokens")))
            .try_parse(self, &mut group)
            .unwrap_or_else(|| {
                Err(CompileError::new(
                    "syntax error",
                    self.fail_place()
                        .unwrap_or_else(|| &self.tokens[0].position),
                ))
            });

        match res {
            Ok(()) => Ok(group.take("value").expect("Failed to get node output")),
            Err(err) => Err(err),
        }
    }

    /// Get place of the last fail
    pub fn fail_place(&self) -> Option<&Position> {
        Some(&self.tokens.get(self.fail_pos?)?.position)
    }

    /// Get current position, sticking to token start.
    ///
    /// If there are skipped symbols at current
    /// position, it returns position after them.
    pub fn start_pos(&self) -> usize {
        self.tokens[self.pos].position.start
    }

    /// Get current position, sticking to token end.
    ///
    /// If there are skipped symbols at current
    /// position, it returns position before them.
    pub fn end_pos(&self) -> usize {
        if self.pos == 0 {
            0
        } else {
            self.tokens[self.pos - 1].position.end
        }
    }

    /// Get code source.
    pub fn source(&self) -> &CodeSource {
        &self.tokens[0].position.source
    }
}

impl Iterator for ParseContext {
    type Item = Token;

    /// Get next token from iterator
    fn next(&mut self) -> Option<Self::Item> {
        let token = self.tokens.get(self.pos).cloned();
        self.pos += 1;

        token
    }
}
