// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::syntax::keywords::words::*;

/// Unary operator.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum UnaryOp {
    Minus,
    Not,
    Length,
}

/// Binary operator.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BinaryOp {
    Power,
    Multiple,
    Divide,
    Mod,
    Overlay,
    Plus,
    Minus,
    And,
    Or,
    Eq,
    NotEq,
    Lt,
    Gt,
    LtEq,
    GtEq,
}

/// Associativity type.
#[derive(Debug, Clone, Copy)]
pub enum Associativity {
    Left,
    Right,
}

/// Operator precedence
pub const OPERATOR_PRECEDENCE: &[(&[BinaryOp], Associativity)] = &[
    (&[BinaryOp::Power], Associativity::Right),
    (
        &[BinaryOp::Multiple, BinaryOp::Divide, BinaryOp::Mod],
        Associativity::Left,
    ),
    (&[BinaryOp::Plus, BinaryOp::Minus], Associativity::Left),
    (&[BinaryOp::Overlay], Associativity::Left),
    (
        &[
            BinaryOp::Eq,
            BinaryOp::NotEq,
            BinaryOp::Lt,
            BinaryOp::LtEq,
            BinaryOp::Gt,
            BinaryOp::GtEq,
        ],
        Associativity::Left,
    ),
    (&[BinaryOp::And, BinaryOp::Or], Associativity::Left),
];

/// Binary operator representations.
pub const BINOP_REPRESENTATIONS: &[(BinaryOp, &str)] = &[
    (BinaryOp::Power, "^"),
    (BinaryOp::Multiple, "*"),
    (BinaryOp::Divide, "/"),
    (BinaryOp::Mod, MOD),
    (BinaryOp::Overlay, "//"),
    (BinaryOp::Plus, "+"),
    (BinaryOp::Minus, "-"),
    (BinaryOp::And, AND),
    (BinaryOp::Or, OR),
    (BinaryOp::Eq, "="),
    (BinaryOp::NotEq, "!="),
    (BinaryOp::Lt, "<"),
    (BinaryOp::LtEq, "<="),
    (BinaryOp::Gt, ">"),
    (BinaryOp::GtEq, ">="),
];

/// Unary operator representations.
pub const UNOP_REPRESENTATIONS: &[(UnaryOp, &str)] = &[
    (UnaryOp::Minus, "-"),
    (UnaryOp::Not, NOT),
    (UnaryOp::Length, "#"),
];
