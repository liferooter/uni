// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::utils::Position;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ConstantValue {
    Integer(i64),
    String(String),
    Boolean(bool),
    None,
}

#[derive(Clone, Debug)]
pub enum Expression {
    Literal(ConstantValue, Position),
    Table(Vec<(String, Expression)>),
    List(Vec<Expression>),
    Variable(String, Position),
    UnaryOp(super::UnaryOp, Box<Expression>, Position),
    BinaryOp(Box<Expression>, super::BinaryOp, Box<Expression>, Position),
    Field(Box<Expression>, String, Position),
    OptionalField(Box<Expression>, String, Position),
    Index(Box<Expression>, Box<Expression>, Position),
    Conditional {
        condition: Box<Expression>,
        body: Box<Expression>,
        else_clause: Box<Expression>,
    },
    LetIn {
        mappings: Vec<(String, Expression)>,
        body: Box<Expression>,
    },
    Function {
        args: Vec<(String, Option<Expression>)>,
        body: Box<Expression>,
    },
    FunctionCall {
        function: Box<Expression>,
        args: Vec<Expression>,
        position: Position,
    },
}
