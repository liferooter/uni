// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    collections::HashMap,
    fmt::{Debug, Display},
    ops::Deref,
    path::{Path, PathBuf},
};
use tokio::{fs, io};

use crate::{
    error,
    errors::PrintableMessage,
    language::{
        self, stdlib, BuiltInFunction, FunctionObject, LazyValue, Scope, TableObject, ToValue,
    },
    parse_tree::ParseContext,
    syntax,
};

/// Runtime object.
pub struct Runtime {
    sources: HashMap<CodeSource, String>,
    std_scope: language::Scope,
}

/// Code source.
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum CodeSource {
    File(PathBuf),
    Module(String),
}

impl Display for CodeSource {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::File(path) => f.write_str(&path.to_string_lossy()),
            Self::Module(name) => f.write_str(&format!("<{name}>")),
        }
    }
}

impl Runtime {
    /// Create default runtime.
    pub async fn default() -> Self {
        Self::new(
            stdlib::BUILTIN_FUNCTIONS.with(|&builtins| builtins),
            stdlib::STDLIB,
        )
        .await
    }

    /// Create new runtime.
    async fn new(
        // builtins: impl IntoIterator<Item = (&str, BuiltInFunction)>,
        builtins: &[(&str, BuiltInFunction)],
        stdlib: impl ToString,
    ) -> Self {
        let builtins = Scope::new(
            None,
            builtins
                .iter()
                .map(|&(name, func)| {
                    (
                        name.to_owned(),
                        LazyValue::Value(FunctionObject::BuiltIn(func).to_value()).into(),
                    )
                })
                .collect(),
        );

        let mut runtime = Self {
            sources: HashMap::new(),
            std_scope: Scope::empty(),
        };

        let stdlib = runtime.evaluate_module("stdlib", &stdlib.to_string()).await;
        let stdlib: TableObject = stdlib.try_into().expect("Standard library must be table");
        runtime.std_scope = Scope::new(Some(builtins), stdlib.fields);

        runtime
    }

    /// Load source file to runtime.
    async fn add_file(&mut self, path: impl AsRef<Path>) -> io::Result<CodeSource> {
        let mut path = path.as_ref().to_path_buf();
        path = fs::canonicalize(path).await?;
        let code = fs::read_to_string(&path).await?;

        self.sources.insert(CodeSource::File(path.clone()), code);

        Ok(CodeSource::File(path))
    }

    /// Load source code of module
    async fn add_module(&mut self, name: impl ToString, code: impl ToString) -> CodeSource {
        self.sources
            .insert(CodeSource::Module(name.to_string()), code.to_string());
        CodeSource::Module(name.to_string())
    }

    /// Get source code.
    pub fn get_code(&self, source: &CodeSource) -> Option<&str> {
        self.sources.get(source).map(Deref::deref)
    }

    /// Evaluate file.
    pub async fn evaluate_file(&mut self, path: impl AsRef<Path>) -> language::Value {
        let source = self.add_file(path).await.unwrap_or_else(error);

        self.evaluate_source(&source).await
    }

    /// Evaluate module.
    pub async fn evaluate_module(
        &mut self,
        name: impl ToString,
        code: impl ToString,
    ) -> language::Value {
        let source = self.add_module(&name.to_string(), &code.to_string()).await;

        self.evaluate_source(&source).await
    }

    /// Evaluate source.
    async fn evaluate_source(&self, source: &CodeSource) -> language::Value {
        let code = &self.sources[source];

        let mut ctx = ParseContext::new(source, code).unwrap_or_else(|err| err.show_error(self));
        let ast = ctx
            .parse(syntax::Expression)
            .unwrap_or_else(|err| err.show_error(self));

        ast.evaluate(&self.std_scope)
            .await
            .unwrap_or_else(|err| err.show_error(self))
    }
}
