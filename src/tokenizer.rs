// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{cmp::min, fmt::Debug};

use lazy_regex::{lazy_regex, Lazy, Regex};

use crate::{errors::CompileError, runtime::CodeSource, utils::Position};

/// Token type.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum TokenType {
    // Ignored token types
    Whitespace,
    Comment,

    // Value token types
    Ident,
    Integer,
    Quoted,

    // Symbol token types
    Operator,
    Comma,
    Bracket,
    Paren,
    Block,

    // End-of-file token
    Eof,
}

/// Token object.
///
/// Contains original parsed code, start index, end index
/// and token type.
#[derive(Debug, Clone)]
pub struct Token {
    pub token_type: TokenType,
    pub position: Position,
    value: String,
}

impl Token {
    fn new(
        token_type: TokenType,
        source: &CodeSource,
        code: &str,
        start: usize,
        end: usize,
    ) -> Self {
        Self {
            token_type,
            position: Position {
                source: source.clone(),
                start,
                end,
            },
            value: code[start..end].to_string(),
        }
    }

    pub fn as_str(&self) -> &str {
        &self.value
    }
}

/// Token regexes.
///
/// Array of pairs in form `(REGEX, TOKEN_TYPE)`.
static TOKEN_REGEXES: [(Lazy<Regex>, TokenType); 10] = [
    (lazy_regex!(r#"\s"#), TokenType::Whitespace),
    (lazy_regex!(r#";.*"#), TokenType::Comment),
    (lazy_regex!(r#"([[:alpha:]]|_)\w*"#), TokenType::Ident),
    (lazy_regex!(r#"[+-]?\d+"#), TokenType::Integer),
    (lazy_regex!(r#"'(\\'|[^'])*'"#), TokenType::Quoted),
    (lazy_regex!(r#"[+\-!#^*/%&|<:=>.?]+"#), TokenType::Operator),
    (lazy_regex!(r#","#), TokenType::Comma),
    (lazy_regex!(r#"[\[\]]"#), TokenType::Bracket),
    (lazy_regex!(r#"[()]"#), TokenType::Paren),
    (lazy_regex!(r#"[{}]"#), TokenType::Block),
];

/// Tokens types to ignore.
static IGNORED_TOKENS: [TokenType; 2] = [TokenType::Whitespace, TokenType::Comment];

/// Tokenize given string.
///
/// Returns vector of tokens or tokenization error.
/// Used for creating parse context.
pub fn tokenize(source: &CodeSource, code: &str) -> Result<Vec<Token>, CompileError> {
    let mut tokens = Vec::new();
    let mut pos: usize = 0;

    loop {
        if pos >= code.len() {
            tokens.push(Token::new(
                TokenType::Eof,
                source,
                code,
                code.len(),
                code.len(),
            ));
            break;
        }

        let mut matched = false;
        for (regex, token_type) in TOKEN_REGEXES.iter() {
            let _match = regex.find_at(code, pos);
            if let Some(_match) = _match {
                if _match.start() == pos {
                    if !IGNORED_TOKENS.contains(token_type) {
                        tokens.push(Token::new(
                            *token_type,
                            source,
                            code,
                            _match.start(),
                            _match.end(),
                        ));
                    }
                    pos = _match.end();
                    matched = true;
                    break;
                }
            }
        }
        if !matched {
            return Err(CompileError::new(
                "tokenization failed",
                &Position {
                    source: source.clone(),
                    start: min(pos, code.len()),
                    end: min(pos + 1, code.len() + 1),
                },
            ));
        }
    }

    Ok(tokens)
}
