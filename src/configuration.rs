// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    collections::HashMap,
    fs,
    path::{Path, PathBuf},
};

#[cfg(unix)]
use std::os::unix::fs::symlink;
#[cfg(windows)]
use std::os::windows::fs::symlink_file as symlink;

use crate::language::{Result, RuntimeError, TableObject, Value, ValueObject};

/// Configuration table
#[derive(Clone)]
pub struct Configuration {
    configuration: TableObject,
    option_overrides: TableObject,
}

impl Configuration {
    pub fn new(value: Value, options: Value) -> Result<Self> {
        Ok(Self {
            configuration: if let Value::Table(table) = value {
                table
            } else {
                return Err(RuntimeError::MustBe {
                    what: "build configuration",
                    correct: "table",
                    incorrect: value.type_name(),
                });
            },
            option_overrides: if let Value::Table(table) = options {
                table
            } else {
                return Err(RuntimeError::MustBe {
                    what: "option overrides",
                    correct: "table",
                    incorrect: options.type_name(),
                });
            },
        })
    }

    pub async fn run_target(&self, target: &str) -> Result<PathBuf> {
        let configuration =
            self.configuration
                .clone()
                .update(TableObject::from_values(HashMap::from([(
                    "options",
                    Value::Table(self.options().await?),
                )])));

        let targets = match configuration.get_or_err("targets").await? {
            Value::Table(table) => table,
            value => {
                return Err(RuntimeError::MustBe {
                    what: "table of targets",
                    correct: "table",
                    incorrect: value.type_name(),
                })
            }
        };

        match targets.get(target).await {
            Some(res) => res.and_then(|val| {
                if let Value::String(string) = val {
                    Ok(PathBuf::from(string.value))
                } else {
                    Err(RuntimeError::MustBe {
                        what: "target path",
                        correct: "string",
                        incorrect: val.type_name(),
                    })
                }
            }),
            None => Err(RuntimeError::TargetNotFound(target.to_owned())),
        }
    }

    pub async fn install_target(&self, target: &str) -> Result<()> {
        let target_prefix = self.run_target(target).await?;
        let install_prefix = match self.options().await?.get_or_err("prefix").await? {
            Value::String(path) => PathBuf::from(path.value),
            value => {
                return Err(RuntimeError::MustBe {
                    what: "prefix path",
                    correct: "string",
                    incorrect: value.type_name(),
                })
            }
        };

        if !install_prefix.exists() {
            fs::create_dir_all(&install_prefix).map_err(RuntimeError::InstallationFailed)?
        }

        let (dirs, files): (Vec<_>, Vec<_>) = iterate_over_files(&target_prefix)
            .map_err(RuntimeError::InstallationFailed)?
            .partition(|path| {
                fs::metadata(path)
                    .map(|metadata| metadata.is_dir())
                    .unwrap_or(false)
            });

        for dir in dirs {
            let relative_path = dir
                .strip_prefix(&target_prefix)
                .expect("Failed to strip path prefix");
            let mut installation_path = install_prefix.clone();
            installation_path.push(relative_path);

            fs::create_dir_all(installation_path).map_err(RuntimeError::InstallationFailed)?;
        }

        for file in files {
            let relative_path = file
                .strip_prefix(&target_prefix)
                .expect("Failed to strip path prefix");
            let mut installation_path = install_prefix.clone();
            installation_path.push(relative_path);

            let metadata = fs::metadata(&file).map_err(RuntimeError::InstallationFailed)?;

            if metadata.is_file() {
                fs::copy(file, installation_path).map_err(RuntimeError::InstallationFailed)?;
            } else if metadata.is_symlink() {
                let original = fs::read_link(&file).map_err(RuntimeError::InstallationFailed)?;
                symlink(original, installation_path).map_err(RuntimeError::InstallationFailed)?;
            }
        }

        Ok(())
    }

    async fn options(&self) -> Result<TableObject> {
        match self.configuration.get("options").await {
            Some(val) => match val? {
                Value::Table(table) => Ok(table.update(self.option_overrides.clone())),
                value => {
                    return Err(RuntimeError::MustBe {
                        what: "table of options",
                        correct: "table",
                        incorrect: value.type_name(),
                    })
                }
            },
            None => Ok(self.option_overrides.clone()),
        }
    }
}

fn iterate_over_files<P: AsRef<Path>>(
    path: P,
) -> std::io::Result<Box<dyn Iterator<Item = PathBuf>>> {
    let metadata = fs::metadata(&path)?;

    if metadata.is_dir() && !metadata.is_symlink() {
        Ok(Box::new(
            fs::read_dir(path)?
                .map(|res| {
                    res.and_then(|dir_entry| {
                        let path = dir_entry.path();
                        Ok(iterate_over_files(&path)?.chain([path]).collect::<Vec<_>>())
                    })
                })
                .collect::<std::io::Result<Vec<Vec<_>>>>()?
                .into_iter()
                .flatten(),
        ))
    } else {
        Ok(Box::new([path.as_ref().to_path_buf()].into_iter()))
    }
}
