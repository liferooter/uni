// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::fmt::Display;

use crate::runtime::CodeSource;

#[macro_export]
macro_rules! error {
    ($($arg:tt)*) => {
        {
            use termion::{
                color::{
                    Fg,
                    Red,
                    Reset
                },
                style::{
                    Bold,
                    Reset as ResetStyle
                }
            };
            eprintln!(
                "{}{}error:{}{} {}",
                Fg(Red),
                Bold,
                ResetStyle,
                Fg(Reset),
                format!(
                    $($arg)*
                )
            );
            std::process::exit(1)
        }
    };
}

pub fn error<T>(obj: impl Display) -> T {
    error!("{}", obj)
}

/// Place in code.
#[derive(Debug, Clone)]
pub struct Position {
    pub source: CodeSource,
    pub start: usize,
    pub end: usize,
}
