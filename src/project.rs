// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{env::current_dir, fs::read_dir, io::Result, path::PathBuf};

/// Find project root
pub fn find_root() -> Result<Option<PathBuf>> {
    let workdir = current_dir()?;
    for dir in workdir.ancestors() {
        for file in read_dir(dir)? {
            let file = file?;
            if file.file_name().to_str() == Some("build.uni") && file.metadata()?.is_file() {
                return Ok(Some(dir.to_path_buf().canonicalize()?));
            }
        }
    }

    Ok(None)
}
