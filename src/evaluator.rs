// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{fs, io};

#[derive(Debug, Clone)]
pub struct Evaluator {
    filename: String,
    code: String,
}

impl Evaluator {
    /// Create new evaluator from filename.
    pub fn from_file(filename: &str) -> io::Result<Self> {
        let code = fs::read_to_string(filename)?;
        Ok(Self {
            filename: filename.to_string(),
            code,
        })
    }

    // Create new evaluator from module name and code.
    pub fn from_code(module_name: &str, code: &str) -> Self {
        Self {
            filename: format!("<{module_name}>"),
            code: code.to_owned(),
        }
    }

    // Run evaluator
}
