// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{cmp::max, process::exit};

use termion::{
    color::{Fg, LightBlack, Red, Reset},
    style::{Bold, Reset as ResetStyle},
};

use crate::{runtime::Runtime, utils::Position};

/// Message that can be printed to user
pub trait PrintableMessage {
    /// Print error to stderr.
    fn print(&self, runtime: &Runtime);

    /// Show printable error and stop executing.
    fn show_error<T>(&self, runtime: &Runtime) -> T {
        self.print(runtime);
        exit(1)
    }
}

/// Compilation error or warning
pub trait LocatedMessage {
    fn message(&self) -> String;
    fn place(&self) -> &Position;
}

impl<T: LocatedMessage> PrintableMessage for T {
    fn print(&self, runtime: &Runtime) {
        eprintln!("{}", self.message());
        let Position { start, end, source } = self.place();
        let code = runtime.get_code(source).expect("Failed to get code");
        let (line, col) = index_to_pos(code, *start);
        eprintln!(
            "{}at {}:{}:{}{}",
            Bold,
            source,
            line + 1,
            col + 1,
            ResetStyle
        );

        let line_text = code.lines().nth(line).unwrap_or_default();
        let old_length = line_text.len();
        let line_text = line_text.trim_start();

        let offset = old_length - line_text.len();

        let line_num = line + 1;
        eprintln!(
            "{} {} |{} {}",
            Fg(LightBlack),
            line_num,
            Fg(Reset),
            line_text.trim_end()
        );
        eprintln!(
            "{} {} | {}{}{}",
            Fg(LightBlack),
            " ".repeat(line_num.to_string().len()),
            " ".repeat(col - offset),
            "^".repeat(max(end - start, 1)),
            Fg(Reset)
        );
        eprintln!();
    }
}

/// Compile error.
///
/// Contains error message and token that error occurred at.
#[derive(Debug)]
pub struct CompileError {
    message: String,
    place: Position,
}

impl CompileError {
    pub fn new(message: &str, place: &Position) -> Self {
        Self {
            message: message.to_string(),
            place: place.clone(),
        }
    }
}

impl LocatedMessage for CompileError {
    fn message(&self) -> String {
        format!(
            "{}{}error:{}{} {}",
            Fg(Red),
            Bold,
            ResetStyle,
            Fg(Reset),
            self.message
        )
    }

    fn place(&self) -> &Position {
        &self.place
    }
}

/// Get line and column from index
fn index_to_pos(text: &str, pos: usize) -> (usize, usize) {
    if pos == 0 {
        (0, 0)
    } else {
        let lines: Vec<&str> = text[..pos].lines().collect();

        let line = lines.len() - 1;
        let col = lines[line].len();
        (line, col)
    }
}
