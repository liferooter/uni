// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{collections::HashMap, path::PathBuf};

use clap::{Parser, Subcommand};
use configuration::Configuration;
use language::{StringObject, TableObject, Value};
use once_cell::sync::OnceCell;

use runtime::Runtime;
use termion::{
    color::{Fg, Reset, White},
    style::{self, Bold},
};
use utils::error;

use crate::{errors::PrintableMessage, language::ValueObject};

mod ast;
mod configuration;
mod errors;
mod language;
mod parse_tree;
mod project;
mod runtime;
mod syntax;
mod tokenizer;
mod utils;

pub static BUILD_DIR: OnceCell<PathBuf> = OnceCell::new();

#[derive(Parser, Debug)]
#[clap(about = "Next generation build system", version = "1.0")]
struct Args {
    #[clap(subcommand)]
    command: Command,
}

#[derive(Subcommand, Debug)]
enum Command {
    /// Build project
    Build {
        /// Install built project
        #[clap(long, short)]
        install: bool,

        /// Installation prefix
        #[clap(long, short)]
        prefix: PathBuf,

        /// Build target
        #[clap(long, short)]
        target: Option<String>,
    },
    /// Evaluate Uni code
    Eval { file: PathBuf },
}

#[tokio::main]
async fn main() {
    let args = Args::parse();
    match args.command {
        Command::Build {
            install,
            prefix,
            target,
        } => {
            let project_root = project::find_root()
                .unwrap_or_else(error)
                .unwrap_or_else(|| error("can't find project root"));
            let mut file = project_root.clone();
            file.push("build.uni");

            BUILD_DIR
                .set(project_root)
                .expect("Failed to set build dir");

            let mut runtime = Runtime::default().await;

            let val = runtime.evaluate_file(file).await;

            let configuration = Configuration::new(
                val,
                TableObject::from_values(HashMap::from([(
                    "prefix",
                    Value::String(StringObject::from_value(
                        prefix
                            .to_str()
                            .unwrap_or_else(|| error("failed to get prefix path")),
                    )),
                )]))
                .into(),
            )
            .unwrap_or_else(|err| err.show_error(&runtime));

            let target = target.as_deref().unwrap_or("build");
            if install {
                configuration
                    .install_target(target)
                    .await
                    .unwrap_or_else(|err| err.show_error(&runtime))
            } else {
                configuration
                    .run_target(target)
                    .await
                    .unwrap_or_else(|err| err.show_error(&runtime));
            }
        }
        Command::Eval { file } => {
            BUILD_DIR
                .set(
                    file.parent()
                        .unwrap_or_else(|| error("failed to find build directory"))
                        .to_path_buf(),
                )
                .expect("Failed to set build dir");

            let mut runtime = Runtime::default().await;
            let val = runtime.evaluate_file(file).await;

            match val.print().await {
                Ok(val) => println!(
                    "{}{}Result:{}{} {}",
                    Fg(White),
                    Bold,
                    style::Reset,
                    Fg(Reset),
                    val
                ),
                Err(err) => err.show_error(&runtime),
            };
        }
    }
}
