// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::{ast, parse_tree::*, syntax::*, utils::Position};

/// Unary operator syntax node.
struct UnaryOp;

impl SyntaxNode for UnaryOp {
    type AstNode = ast::UnaryOp;

    fn grammar(&self) -> Node {
        node!(AnyOf(
            ast::UNOP_REPRESENTATIONS
                .iter()
                .map(|&(op, representation)| Box::new(sequence!(
                    representation,
                    SetValue("value", op)
                )) as Box<dyn ParseNode>)
                .collect::<Vec<_>>()
        ))
    }
}

/// Binary operator syntax node.
struct BinaryOp;

impl SyntaxNode for BinaryOp {
    type AstNode = ast::BinaryOp;

    fn grammar(&self) -> Node {
        node!(any_of!(node!(AnyOf(
            ast::BINOP_REPRESENTATIONS
                .iter()
                .map(|&(op, representation)| Box::new(sequence!(
                    representation,
                    SetValue("value", op)
                )) as Box<dyn ParseNode>)
                .collect::<Vec<_>>()
        ))))
    }
}

#[derive(Debug, Clone)]
enum ExpressionSuffix {
    Index {
        index: ast::Expression,
        position: Position,
    },
    OptionalField {
        name: String,
        position: Position,
    },
    Field {
        name: String,
        position: Position,
    },
    Call {
        args: Vec<ast::Expression>,
        position: Position,
    },
}

/// Simple expression syntax node.
///
/// Matches expression without binary operators.
struct SimpleExpression;

impl SyntaxNode for SimpleExpression {
    type AstNode = ast::Expression;

    fn grammar(&self) -> Node {
        node!(sequence!(
            any_of!(
                // Conditional expression
                (
                    "value",
                    Group {
                        grammar: sequence!(
                            IF,
                            ("condition", Expression).or_err("condition is expected after `if`"),
                            THEN,
                            ("body", Expression),
                            ELSE.or_err("`else` clause is expected after `if` body"),
                            ("else_clause", Expression).or_err("`else` clause must have body")
                        ),
                        callback: |mut group| Self::AstNode::Conditional {
                            condition: Box::new(group.take("condition").unwrap()),
                            body: Box::new(group.take("body").unwrap()),
                            else_clause: Box::new(group.take("else_clause").unwrap()),
                        }
                    }
                ),
                // Let-in expression
                (
                    "value",
                    Group {
                        grammar: sequence!(
                            LET,
                            ("mappings", Pairs),
                            IN,
                            ("body", Expression).or_err("`let` expression must have body ")
                        ),
                        callback: |mut group| Self::AstNode::LetIn {
                            mappings: group.take("mappings").unwrap(),
                            body: Box::new(group.take("body").unwrap())
                        }
                    }
                ),
                // Unary operator
                (
                    "value",
                    Group {
                        grammar: sequence!(
                            WithPosition("pos", ("operator", UnaryOp)),
                            ("expr", Self).or_err("expected expression after unary operator")
                        ),
                        callback: |mut group| Self::AstNode::UnaryOp(
                            group.take("operator").unwrap(),
                            Box::new(group.take("expr").unwrap()),
                            group.take("pos").unwrap()
                        )
                    }
                ),
                // Table
                ("value", Table),
                // Array
                ("value", List),
                // Literal
                ("value", ConstantValue),
                // Variable
                ("value", Variable),
                // Function definition
                ("value", Function),
                // Expression in parens
                sequence!(
                    "(",
                    ("value", Expression).or_err("expected expression between parentheses"),
                    ")"
                )
            ),
            // Expression suffixes
            ZeroOrMore(any_of!(
                // Optional field
                Repetition(
                    "suffixes",
                    Group {
                        grammar: sequence!(
                            ".",
                            WithPosition("pos", Ident("field")).or_err("expected field name"),
                            "?"
                        ),
                        callback: |mut group| ExpressionSuffix::OptionalField {
                            name: group.take("field").unwrap(),
                            position: group.take("pos").unwrap()
                        }
                    }
                ),
                // Field
                Repetition(
                    "suffixes",
                    Group {
                        grammar: sequence!(
                            ".",
                            WithPosition("pos", Ident("field")).or_err("expected field name")
                        ),
                        callback: |mut group| ExpressionSuffix::Field {
                            name: group.take("field").unwrap(),
                            position: group.take("pos").unwrap()
                        }
                    }
                ),
                // Index
                Repetition(
                    "suffixes",
                    Group {
                        grammar: sequence!(
                            WithPosition("pos", "["),
                            ("index", Expression).or_err("expected index expression"),
                            "]"
                        ),
                        callback: |mut group| ExpressionSuffix::Index {
                            index: group.take("index").unwrap(),
                            position: group.take("pos").unwrap()
                        }
                    }
                ),
                // Function call
                Repetition(
                    "suffixes",
                    Group {
                        grammar: sequence!(
                            WithPosition("pos", "("),
                            any_of!(
                                sequence!(("args", Values), ")"),
                                sequence!(
                                    (
                                        "args",
                                        Group {
                                            grammar: ("fields", Pairs),
                                            callback: |mut group| vec![ast::Expression::Table(
                                                group.take("fields").unwrap()
                                            )]
                                        }
                                    ),
                                    ")"
                                )
                            )
                        ),
                        callback: |mut group| ExpressionSuffix::Call {
                            args: group.take("args").unwrap_or_default(),
                            position: group.take("pos").unwrap()
                        }
                    }
                )
            ))
        ))
    }

    fn construct(mut group: CaptureGroup) -> Self::AstNode {
        let value = group.take("value").unwrap();
        let suffixes: Vec<_> = group.take("suffixes").unwrap_or_default();

        suffixes
            .into_iter()
            .fold(value, |val, suffix| match suffix {
                ExpressionSuffix::Field { name, position } => {
                    ast::Expression::Field(Box::new(val), name, position)
                }
                ExpressionSuffix::OptionalField { name, position } => {
                    ast::Expression::OptionalField(Box::new(val), name, position)
                }
                ExpressionSuffix::Index { index, position } => {
                    ast::Expression::Index(Box::new(val), Box::new(index), position)
                }
                ExpressionSuffix::Call { args, position } => ast::Expression::FunctionCall {
                    function: Box::new(val),
                    args,
                    position,
                },
            })
    }
}

/// Expression syntax node.
pub struct Expression;

impl SyntaxNode for Expression {
    type AstNode = ast::Expression;

    fn grammar(&self) -> Node {
        node!(sequence!(
            Repetition("expressions", SimpleExpression),
            ZeroOrMore(sequence!(
                Repetition(
                    "operators",
                    Group {
                        grammar: WithPosition("pos", ("op", BinaryOp)),
                        callback: |mut group| (
                            group.take::<ast::BinaryOp>("op").unwrap(),
                            group.take::<Position>("pos").unwrap()
                        )
                    }
                ),
                Repetition("expressions", SimpleExpression)
            ))
        ))
    }

    fn construct(mut group: CaptureGroup) -> Self::AstNode {
        let mut expressions: Vec<Self::AstNode> = group.take("expressions").unwrap();
        let mut operators: Vec<(ast::BinaryOp, Position)> =
            group.take("operators").unwrap_or_default();

        for (binops, associativity) in ast::OPERATOR_PRECEDENCE.iter() {
            match associativity {
                ast::Associativity::Left => {
                    while let Some((index, _)) = operators
                        .iter()
                        .enumerate()
                        .find(|&(_, (op, _))| binops.contains(op))
                    {
                        let left = expressions.remove(index);
                        let right = expressions.remove(index);
                        let (op, pos) = operators.remove(index);
                        expressions.insert(
                            index,
                            Self::AstNode::BinaryOp(Box::new(left), op, Box::new(right), pos),
                        );
                    }
                }
                ast::Associativity::Right => {
                    while let Some((index, _)) = operators
                        .iter()
                        .enumerate()
                        .rfind(|&(_, (op, _))| binops.contains(op))
                    {
                        let left = expressions.remove(index);
                        let right = expressions.remove(index);
                        let (op, pos) = operators.remove(index);
                        expressions.insert(
                            index,
                            Self::AstNode::BinaryOp(Box::new(left), op, Box::new(right), pos),
                        );
                    }
                }
            }
        }

        expressions.remove(0)
    }
}
