// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::parse_tree::*;

/// Syntax node.
///
/// Matches its grammar and returns its AST node.
pub trait SyntaxNode: ParseNode {
    type AstNode: 'static;

    /// Get syntax node grammar.
    fn grammar(&self) -> Node;

    /// Construct AST node from captured data.
    fn construct(mut group: CaptureGroup) -> Self::AstNode {
        group
            .take("value")
            .expect("Failed to get value from AST node")
    }
}

impl<T, V> ValueNode for T
where
    T: SyntaxNode<AstNode = V>,
    V: 'static,
{
    type ValueType = V;

    fn _parse(&self, ctx: &mut ParseContext) -> Option<Result<V, crate::errors::CompileError>> {
        Group {
            grammar: self.grammar(),
            callback: Self::construct,
        }
        .parse_to_val(ctx)
    }
}
