// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::{ast, parse_tree::*, syntax::*};

use super::SyntaxNode;

/// Table syntax node.
pub struct Table;

impl SyntaxNode for Table {
    type AstNode = ast::Expression;

    fn grammar(&self) -> Node {
        node!(sequence!("{", ("fields", Pairs), "}"))
    }

    fn construct(mut group: CaptureGroup) -> Self::AstNode {
        Self::AstNode::Table(group.take("fields").unwrap_or_default())
    }
}

/// List syntax node.
pub struct List;

impl SyntaxNode for List {
    type AstNode = ast::Expression;

    fn grammar(&self) -> Node {
        node!(sequence!("[", ("elems", Values), "]"))
    }

    fn construct(mut group: CaptureGroup) -> Self::AstNode {
        Self::AstNode::List(group.take("elems").unwrap_or_default())
    }
}
