// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::parse_tree::*;
use crate::syntax::*;
use crate::{ast, node, sequence};

use super::SyntaxNode;

/// Key-value pair as in a table
pub struct KeyValue;

impl SyntaxNode for KeyValue {
    type AstNode = (String, ast::Expression);

    fn grammar(&self) -> crate::parse_tree::Node {
        node!(sequence!(Ident("key"), ":", ("value", Expression)))
    }

    fn construct(mut group: CaptureGroup) -> Self::AstNode {
        (group.take("key").unwrap(), group.take("value").unwrap())
    }
}

/// Comma-separated list of key-value pairs
pub struct Pairs;

impl SyntaxNode for Pairs {
    type AstNode = Vec<(String, ast::Expression)>;

    fn grammar(&self) -> Node {
        node!(sequence!(
            SetValue("value", Self::AstNode::new()),
            Delimited(Repetition("value", KeyValue), ","),
            Optional(",")
        ))
    }
}

/// Comma-separated values
pub struct Values;

impl SyntaxNode for Values {
    type AstNode = Vec<ast::Expression>;

    fn grammar(&self) -> Node {
        node!(sequence!(
            SetValue("value", Self::AstNode::new()),
            Delimited(Repetition("value", Expression), ","),
            Optional(",")
        ))
    }

    fn construct(mut group: CaptureGroup) -> Self::AstNode {
        group
            .take("value")
            .expect("Failed to get value from AST node")
    }
}
