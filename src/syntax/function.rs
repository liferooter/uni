// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::{ast, parse_tree::*, syntax::*};

use super::SyntaxNode;

/// Function syntax node.
pub struct Function;

impl SyntaxNode for Function {
    type AstNode = ast::Expression;

    fn grammar(&self) -> crate::parse_tree::Node {
        node!(sequence!(
            "(",
            Delimited(
                Repetition(
                    "args",
                    Group {
                        grammar: sequence!(
                            Ident("arg_name"),
                            Optional(sequence!(":", ("default_val", Expression)))
                        ),
                        callback: |mut group| (
                            group.take::<String>("arg_name").unwrap(),
                            group.take::<Self::AstNode>("default_val")
                        )
                    }
                ),
                ","
            ),
            ")",
            "=>",
            ("body", Expression).or_err("function must have body")
        ))
    }

    fn construct(mut group: CaptureGroup) -> Self::AstNode {
        let args = group.take("args").unwrap_or_default();
        Self::AstNode::Function {
            args,
            body: Box::new(group.take("body").unwrap()),
        }
    }
}
