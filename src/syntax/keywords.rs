// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

macro_rules! collect_keywords {
    (
        $($name:ident: $val:literal,)*
    ) => {
        pub mod words {
            $(
                pub const $name: &str = $val;
            )*
        }

        pub const KEYWORDS: &[&str] = &[$(
            words::$name,
        )*];
    };
}

collect_keywords! {
    // Boolean values
    TRUE: "true",
    FALSE: "false",

    // None value
    NONE: "none",

    // Operators
    AND: "and",
    OR: "or",
    MOD: "mod",
    NOT: "not",

    // Let-in construction
    LET: "let",
    IN: "in",

    // If-else consturction
    IF: "if",
    THEN: "then",
    ELSE: "else",
}
