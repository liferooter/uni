// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::{ast, parse_tree::*, syntax::*};

use super::SyntaxNode;

/// Constant value syntax node.
pub struct ConstantValue;

impl SyntaxNode for ConstantValue {
    type AstNode = ast::Expression;

    fn grammar(&self) -> Node {
        node!(WithPosition(
            "pos",
            any_of!(
                // Integer literal
                (
                    "constant",
                    Group {
                        grammar: Integer("integer"),
                        callback: |mut group| ast::ConstantValue::Integer(
                            group
                                .take("integer")
                                .expect("Failed to get integer from literal")
                        )
                    },
                ),
                // String literal
                (
                    "constant",
                    Group {
                        grammar: Quoted("string"),
                        callback: |mut group| ast::ConstantValue::String(
                            group
                                .take("string")
                                .expect("Failed to get string from literal")
                        )
                    }
                ),
                // Boolean literal
                sequence!(
                    TRUE,
                    SetValue("constant", ast::ConstantValue::Boolean(true))
                ),
                sequence!(
                    FALSE,
                    SetValue("constant", ast::ConstantValue::Boolean(false))
                ),
                // None
                sequence!(NONE, SetValue("constant", ast::ConstantValue::None))
            )
        ))
    }

    fn construct(mut group: CaptureGroup) -> Self::AstNode {
        Self::AstNode::Literal(
            group
                .take("constant")
                .expect("Failed to get constant value"),
            group.take("pos").unwrap(),
        )
    }
}

/// Variable syntax node.
pub struct Variable;

impl SyntaxNode for Variable {
    type AstNode = ast::Expression;

    fn grammar(&self) -> Node {
        node!(WithPosition("pos", Ident("variable_name")))
    }

    fn construct(mut group: CaptureGroup) -> Self::AstNode {
        Self::AstNode::Variable(
            group
                .take("variable_name")
                .expect("Failed to get variable name"),
            group.take("pos").unwrap(),
        )
    }
}
