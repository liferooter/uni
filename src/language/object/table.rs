// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::collections::HashMap;

use async_trait::async_trait;
use futures::future::join_all;

use crate::{
    ast::Expression,
    language::{
        errors::{Result, RuntimeError},
        lazy::LazyValue,
        scope::Scope,
        value::{Value, ValueObject},
        Closure, ToValue,
    },
};

/// Uni table class
#[derive(Clone, Debug)]
pub struct TableObject {
    pub fields: HashMap<String, Closure>,
}

#[async_trait(?Send)]
impl ValueObject for TableObject {
    async fn print(&self) -> Result<String> {
        Ok(format!(
            "{{{}}}",
            join_all(
                self.values()
                    .await?
                    .into_iter()
                    .map(|(key, value)| async move {
                        Result::Ok(format!("{}: {}", key, value.print().await?))
                    })
            )
            .await
            .into_iter()
            .collect::<Result<Vec<_>>>()?
            .join(", ")
        ))
    }

    async fn cmp(&self, _: &Self) -> Result<Option<std::cmp::Ordering>> {
        Ok(None)
    }

    fn to_boolean(&self) -> bool {
        self.fields.is_empty()
    }

    fn type_name(&self) -> &'static str {
        "table"
    }

    fn into_value(self) -> Value {
        Value::Table(self)
    }
}

impl From<TableObject> for Value {
    fn from(table: TableObject) -> Self {
        Self::Table(table)
    }
}

impl TableObject {
    /// Create new table from scope and key-value pairs.
    pub fn new(
        scope: &Scope,
        fields: impl IntoIterator<Item = (impl ToString, Expression)>,
    ) -> Self {
        Self {
            fields: fields
                .into_iter()
                .map(|(key, expr)| (key.to_string(), Closure::Inherit(expr, scope.clone())))
                .collect(),
        }
    }

    pub fn from_values(fields: impl IntoIterator<Item = (impl ToString, impl ToValue)>) -> Self {
        Self {
            fields: fields
                .into_iter()
                .map(|(key, val)| {
                    (
                        key.to_string(),
                        Closure::Value(LazyValue::Value(val.to_value())),
                    )
                })
                .collect(),
        }
    }

    /// Merge two tables.
    pub fn update(self, update: Self) -> Self {
        Self {
            fields: self.fields.into_iter().chain(update.fields).collect(),
        }
    }

    /// Get iterator over fields in form of key-value pairs.
    pub async fn values(&self) -> Result<HashMap<String, Value>> {
        let fields = join_all(self.fields.keys().map(|name| async move {
            Ok((
                name.clone(),
                self.get(name)
                    .await
                    .expect("Table doesn't contain its own field")?,
            ))
        }))
        .await
        .into_iter()
        .collect::<Result<_>>()?;

        Ok(fields)
    }

    /// Get field by its name.
    pub async fn get_or_err(&self, field: &str) -> Result<Value> {
        self.get(field)
            .await
            .unwrap_or_else(|| Err(RuntimeError::FieldNotFound(field.to_string())))
    }

    fn scope(&self) -> Scope {
        Scope::new(None, self.fields.clone())
    }

    /// Get field by its name, or `None` if not exist.
    pub async fn get(&self, field: &str) -> Option<Result<Value>> {
        self.scope().get(field).await
    }
}
