// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{cmp::Ordering, ops::Add};

use async_trait::async_trait;
use futures::{future::join_all, stream, Stream, StreamExt};

use crate::{
    ast::Expression,
    language::{
        errors::{Result, RuntimeError},
        lazy::LazyValue,
        scope::Scope,
        value::{Value, ValueObject},
    },
};

#[derive(Clone, Debug)]
pub struct ListObject {
    pub list: Vec<LazyValue>,
}

impl Add for ListObject {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        Self {
            list: self.list.into_iter().chain(rhs.list).collect(),
        }
    }
}

impl From<ListObject> for Value {
    fn from(list: ListObject) -> Self {
        Self::List(list)
    }
}

#[async_trait(?Send)]
impl ValueObject for ListObject {
    async fn print(&self) -> Result<String> {
        Ok(format!(
            "[{}]",
            join_all(
                self.values()
                    .await?
                    .iter()
                    .map(|value| async { value.print().await })
                    .collect::<Vec<_>>()
            )
            .await
            .into_iter()
            .collect::<Result<Vec<_>>>()?
            .join(", ")
        ))
    }

    async fn cmp(&self, other: &Self) -> Result<Option<Ordering>> {
        let mut iter = self.list.iter();
        let mut other_iter = other.list.iter();
        Ok(loop {
            match (iter.next(), other_iter.next()) {
                (None, None) => break Some(Ordering::Equal),
                (Some(_), None) => break Some(Ordering::Greater),
                (None, Some(_)) => break Some(Ordering::Less),
                (Some(val), Some(other_val)) => {
                    let val: Value = val.get().await?;
                    let other_val: Value = other_val.get().await?;

                    match val.cmp(&other_val).await? {
                        None => break None,
                        Some(Ordering::Equal) => continue,
                        other => break other,
                    }
                }
            }
        })
    }

    fn to_boolean(&self) -> bool {
        self.list.is_empty()
    }

    fn type_name(&self) -> &'static str {
        "list"
    }

    fn into_value(self) -> Value {
        Value::List(self)
    }
}

impl ListObject {
    /// Create new list object from scope and list of expressions.
    pub fn new(scope: &Scope, list: impl IntoIterator<Item = Expression>) -> Self {
        Self {
            list: list
                .into_iter()
                .map(|expr| LazyValue::Expression(expr, scope.clone()))
                .collect(),
        }
    }

    /// Create new list object from list of values
    pub fn from_values(values: Vec<Value>) -> Self {
        Self {
            list: values.into_iter().map(LazyValue::Value).collect(),
        }
    }

    /// Get element from list.
    pub async fn get(&self, index: i64) -> Result<Value> {
        self.list
            .get(if index >= 0 {
                index as usize
            } else {
                (self.list.len() as i64 + index) as usize
            })
            .ok_or(RuntimeError::IndexOutOfRange)?
            .get()
            .await
    }

    /// Get evaluated values of list.
    pub async fn values(&self) -> Result<Vec<Value>> {
        let mut res = Vec::new();
        for elem in &self.list {
            res.push(elem.get().await?)
        }

        Ok(res)
    }

    /// Get stream returning values of list.
    pub fn values_stream(&self) -> impl Stream<Item = Result<Value>> {
        stream::iter(self.list.clone()).then(|val| async move { val.get().await })
    }

    /// Get list's length.
    pub fn len(&self) -> usize {
        self.list.len()
    }
}
