// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::path::{Path, PathBuf};

use async_trait::async_trait;
use lazy_regex::{lazy_regex, Lazy, Regex};
use sha2::{Digest, Sha256};
use tokio::{fs, io::AsyncReadExt};

use crate::language::{
    errors::{Result, RuntimeError},
    scope::Scope,
    value::ValueObject,
    Value,
};

/// Magic value that will be replaced with target out path.
const OUT: &str = "\0";

/// String object.
///
/// It contains list of files it depends on.
#[derive(Debug, Clone, Hash)]
pub struct StringObject {
    /// Value of the string object.
    pub value: String,
    /// List of files it depends on.
    pub deps: Vec<PathBuf>,
}

impl<S: Into<String>> From<S> for StringObject {
    fn from(s: S) -> Self {
        StringObject::from_value(s.into())
    }
}

#[async_trait(?Send)]
impl ValueObject for StringObject {
    async fn to_string(&self) -> Result<StringObject> {
        Ok(self.clone())
    }

    async fn print(&self) -> Result<String> {
        Ok(format!("'{}'", Self::escape(&self.value)))
    }

    async fn cmp(&self, other: &Self) -> Result<Option<std::cmp::Ordering>> {
        Ok(Some(self.value.cmp(&other.value)))
    }

    fn to_boolean(&self) -> bool {
        !self.value.is_empty()
    }

    fn type_name(&self) -> &'static str {
        "string"
    }

    fn into_value(self) -> Value {
        Value::String(self)
    }
}

/// Array of escape sequences.
static ESCAPE_SEQUENCES: [(&str, &str); 7] = [
    ("\0", "$out"),
    ("$", r"\$"),
    ("\\", r"\\"),
    ("\n", r"\n"),
    ("\r", r"\r"),
    ("\t", r"\t"),
    ("'", r"\'"),
];

impl StringObject {
    /// Create new string object from value.
    pub fn from_value(value: impl Into<String>) -> Self {
        Self {
            value: value.into(),
            deps: Vec::new(),
        }
    }

    /// Create new string object from file name.
    pub fn from_filename(path: StringObject) -> Self {
        let mut path_buf = crate::BUILD_DIR.get().expect("No build dir set").clone();
        path_buf.push(&path.value);

        let mut deps = path.deps;
        deps.push(path_buf);

        Self {
            value: path.value,
            deps,
        }
    }

    /// Create new string object from path.
    pub fn from_path(path: impl AsRef<Path>) -> Self {
        let path = path.as_ref().to_path_buf();

        Self {
            value: path.to_string_lossy().to_string(),
            deps: vec![path],
        }
    }

    /// Create new string object from string literal and its scope.
    pub async fn from_literal(s: &str, scope: &Scope) -> Result<Self> {
        let (string, deps) = Self::substitude(s, scope).await?;
        Ok(Self {
            value: Self::unescape(&string),
            deps,
        })
    }

    /// Substitude `$variable_name` sequences with variable values.
    ///
    /// Returns result string and shadow string.
    async fn substitude(s: &str, scope: &Scope) -> Result<(String, Vec<PathBuf>)> {
        static REGEX: Lazy<Regex> = lazy_regex!(
            r"^([^\\]|\\\\)*(?P<match>\$(\{(?P<name_in_brackets>\w+)\}|(?P<name>\w+)))"
        );

        let mut text = s.to_owned();
        let mut res_deps = Vec::new();

        while let Some(captures) = REGEX.captures(&text) {
            let name = captures
                .name("name")
                .or_else(|| captures.name("name_in_brackets"))
                .expect("Failed to get capture group `name`")
                .as_str()
                .to_string();
            let value = if name == "out" {
                // Insert magic symbol used for output path
                String::from(OUT)
            } else {
                let Self { value, deps } = scope.get_or_err(&name).await?.to_string().await?;

                res_deps.extend(deps);

                Self::escape(&value)
            };

            let _match = captures
                .name("match")
                .expect("Failed to get capture group `match`");

            text = text[0.._match.start()].to_string() + &value + &text[_match.end()..text.len()];
        }
        Ok((text, res_deps))
    }

    /// Replace special symbols by escape sequences.
    pub fn escape(s: &str) -> String {
        let mut text = s.to_owned();
        for (symbol, sequence) in ESCAPE_SEQUENCES {
            text = text.replace(symbol, sequence);
        }

        text
    }

    /// Replace escape sequences by special symbols.
    fn unescape(s: &str) -> String {
        let mut text = s.to_owned();
        for (symbol, sequence) in ESCAPE_SEQUENCES {
            text = text.replace(sequence, symbol);
        }

        text
    }

    /// Concatenate two strings.
    ///
    /// This function concatenates two strings and its shadows.
    pub fn concat(self, other: Self) -> Self {
        Self {
            value: self.value + &other.value,
            deps: self.deps.into_iter().chain(other.deps).collect(),
        }
    }

    /// Calculate hash of string and its dependencies.
    pub async fn hash(&self) -> Result<String> {
        let mut hasher = Sha256::new();
        hasher.update(&self.value);

        for path in &self.deps {
            let mut file = fs::File::open(&path)
                .await
                .map_err(|err| RuntimeError::FileError(path.clone(), err))?;
            let mut buffer = [0u8; 64];
            let mut file_hasher = Sha256::new();
            while file
                .read(&mut buffer)
                .await
                .map_err(|err| RuntimeError::FileError(path.clone(), err))?
                != 0
            {
                file_hasher.update(buffer);
            }
            hasher.update(path.to_string_lossy().as_bytes());
            hasher.update(file_hasher.finalize());
        }

        Ok(hex::encode(hasher.finalize()))
    }
}
