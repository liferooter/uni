// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{cmp::Ordering, fmt::Debug, future::Future, pin::Pin};

use async_recursion::async_recursion;
use async_trait::async_trait;

use crate::{
    ast::Expression,
    language::{
        errors::Result,
        lazy::LazyValue,
        scope::Scope,
        value::{Value, ValueObject},
        Closure, RuntimeError,
    },
};

pub type DynamicFuture<T> = Pin<Box<dyn Future<Output = T>>>;
pub type BuiltInFunction = &'static dyn Fn(Vec<LazyValue>) -> DynamicFuture<Result<Value>>;

/// Uni function object.
#[derive(Clone)]
pub enum FunctionObject {
    Expression {
        args: Vec<String>,
        scope: Scope,
        body: Expression,
    },
    BuiltIn(BuiltInFunction),
}

impl Debug for FunctionObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            Self::Expression { .. } => "<function>",
            Self::BuiltIn(_) => "<built-in function>",
        })
    }
}

impl From<FunctionObject> for Value {
    fn from(func: FunctionObject) -> Self {
        Self::Function(func)
    }
}

#[async_trait(?Send)]
impl ValueObject for FunctionObject {
    async fn print(&self) -> Result<String> {
        Ok(String::from("<function>"))
    }

    async fn cmp(&self, _: &Self) -> Result<Option<Ordering>> {
        Ok(None)
    }

    fn to_boolean(&self) -> bool {
        true
    }

    fn type_name(&self) -> &'static str {
        "function"
    }

    fn into_value(self) -> Value {
        Value::Function(self)
    }
}

impl FunctionObject {
    /// Create new function object from scope, list of arguemtns and function body.
    pub fn new(scope: &Scope, args: Vec<(String, Option<Expression>)>, body: Expression) -> Self {
        Self::Expression {
            args: args.iter().map(|(key, _)| key.clone()).collect(),
            scope: Scope::new(
                Some(scope.clone()),
                args.iter()
                    .cloned()
                    .filter_map(|(name, expr)| {
                        Some((
                            name,
                            Closure::Value(LazyValue::Expression(expr?, scope.clone())),
                        ))
                    })
                    .collect(),
            ),
            body,
        }
    }

    /// Call function.
    #[async_recursion(?Send)]
    pub async fn call(&self, args: Vec<LazyValue>) -> Result<Value> {
        match self {
            Self::Expression {
                args: arg_names,
                body,
                scope,
            } => {
                if args.len() != arg_names.len() {
                    return Err(RuntimeError::WrongArgumentsNumber {
                        recieves: arg_names.len(),
                        passed: args.len(),
                    });
                }

                body.evaluate(&Scope::new(
                    Some(scope.clone()),
                    arg_names
                        .clone()
                        .into_iter()
                        .zip(args.into_iter().map(Closure::from))
                        .collect(),
                ))
                .await
            }
            Self::BuiltIn(function) => function(args).await,
        }
    }
}
