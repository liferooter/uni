// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

mod function;
mod list;
mod string;
mod table;

pub use function::FunctionObject;
pub use list::ListObject;
pub use string::StringObject;
pub use table::TableObject;

pub use function::{BuiltInFunction, DynamicFuture};
