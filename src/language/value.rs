// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{cmp::Ordering, collections::HashMap, path::Path};

use async_trait::async_trait;

use super::{
    errors::{Result, RuntimeError},
    FunctionObject, ListObject, StringObject, TableObject,
};

/// Language value type.
#[derive(Clone, Debug)]
pub enum Value {
    Integer(i64),
    Boolean(bool),
    String(StringObject),
    Function(FunctionObject),
    List(ListObject),
    Table(TableObject),
    None,
}

pub trait ToValue {
    fn to_value(&self) -> Value;
}

impl<V: ValueObject + Clone> ToValue for V {
    fn to_value(&self) -> Value {
        ValueObject::into_value(self.clone())
    }
}

impl ToValue for i32 {
    fn to_value(&self) -> Value {
        Value::Integer(*self as i64)
    }
}

impl ToValue for bool {
    fn to_value(&self) -> Value {
        Value::Boolean(*self)
    }
}

impl ToValue for String {
    fn to_value(&self) -> Value {
        StringObject::from(self).into_value()
    }
}

impl ToValue for &Path {
    fn to_value(&self) -> Value {
        StringObject::from_path(self).into_value()
    }
}

impl ToValue for Vec<Box<dyn ToValue>> {
    fn to_value(&self) -> Value {
        ListObject::from_values(self.iter().map(|val| val.to_value()).collect()).into_value()
    }
}

impl ToValue for HashMap<String, Box<dyn ToValue>> {
    fn to_value(&self) -> Value {
        TableObject::from_values(self.iter().map(|(key, val)| (key.clone(), val.to_value())))
            .into_value()
    }
}

impl<V: ToValue> ToValue for Option<V> {
    fn to_value(&self) -> Value {
        if let Some(val) = self {
            val.to_value()
        } else {
            Value::None
        }
    }
}

impl ToValue for () {
    fn to_value(&self) -> Value {
        Value::None
    }
}

/// Value object trait.
///
/// Contains methods shared by all value object types.
#[async_trait(?Send)]
pub trait ValueObject {
    /// Convert value to string.
    async fn to_string(&self) -> Result<StringObject> {
        Err(RuntimeError::Cant("convert to string", self.type_name()))
    }

    /// Print value.
    async fn print(&self) -> Result<String> {
        Ok(self.to_string().await?.value)
    }

    async fn cmp(&self, other: &Self) -> Result<Option<Ordering>>;
    async fn eq(&self, other: &Self) -> Result<bool> {
        Ok(self.cmp(other).await? == Some(Ordering::Equal))
    }

    fn to_boolean(&self) -> bool;
    fn type_name(&self) -> &'static str;

    fn into_value(self) -> Value;
}

#[async_trait(?Send)]
impl ValueObject for Value {
    async fn to_string(&self) -> Result<StringObject> {
        Ok(match self {
            Self::Boolean(val) => StringObject::from_value(val.to_string()),
            Self::Integer(val) => StringObject::from_value(val.to_string()),
            Self::None => StringObject::from_value(""),
            Self::String(val) => val.to_string().await?,
            Self::List(val) => val.to_string().await?,
            Self::Table(val) => val.to_string().await?,
            Self::Function(val) => val.to_string().await?,
        })
    }

    async fn print(&self) -> Result<String> {
        Ok(match self {
            Self::Boolean(val) => val.to_string(),
            Self::Integer(val) => val.to_string(),
            Self::None => String::from("none"),
            Self::String(val) => val.print().await?,
            Self::List(val) => val.print().await?,
            Self::Table(val) => val.print().await?,
            Self::Function(val) => val.print().await?,
        })
    }

    async fn cmp(&self, other: &Self) -> Result<Option<Ordering>> {
        Ok(match (self, other) {
            (Self::Boolean(left), Self::Boolean(right)) => Some(left.cmp(right)),
            (Self::Integer(left), Self::Integer(right)) => Some(left.cmp(right)),
            (Self::String(left), Self::String(right)) => left.cmp(right).await?,
            (Self::List(left), Self::List(right)) => left.cmp(right).await?,
            (Self::Table(left), Self::Table(right)) => left.cmp(right).await?,
            (Self::Function(left), Self::Function(right)) => left.cmp(right).await?,
            (Self::None, Self::None) => Some(Ordering::Equal),
            _ => None,
        })
    }

    fn to_boolean(&self) -> bool {
        match self {
            &Self::Boolean(val) => val,
            &Self::Integer(val) => val != 0,
            Self::None => false,
            Self::String(val) => val.to_boolean(),
            Self::List(val) => val.to_boolean(),
            Self::Table(val) => val.to_boolean(),
            Self::Function(val) => val.to_boolean(),
        }
    }

    fn type_name(&self) -> &'static str {
        match self {
            Self::Integer(_) => "integer",
            Self::Boolean(_) => "boolean",
            Self::String(val) => val.type_name(),
            Self::List(val) => val.type_name(),
            Self::Table(val) => val.type_name(),
            Self::Function(val) => val.type_name(),
            Self::None => "none",
        }
    }

    fn into_value(self) -> Value {
        self
    }
}

impl TryFrom<Value> for bool {
    type Error = Value;

    fn try_from(value: Value) -> std::result::Result<Self, Self::Error> {
        if let Value::Boolean(val) = value {
            Ok(val)
        } else {
            Err(value)
        }
    }
}

impl TryFrom<Value> for i64 {
    type Error = Value;

    fn try_from(value: Value) -> std::result::Result<Self, Self::Error> {
        if let Value::Integer(val) = value {
            Ok(val)
        } else {
            Err(value)
        }
    }
}

impl TryFrom<Value> for StringObject {
    type Error = Value;

    fn try_from(value: Value) -> std::result::Result<Self, Self::Error> {
        if let Value::String(val) = value {
            Ok(val)
        } else {
            Err(value)
        }
    }
}

impl TryFrom<Value> for ListObject {
    type Error = Value;

    fn try_from(value: Value) -> std::result::Result<Self, Self::Error> {
        if let Value::List(val) = value {
            Ok(val)
        } else {
            Err(value)
        }
    }
}

impl TryFrom<Value> for TableObject {
    type Error = Value;

    fn try_from(value: Value) -> std::result::Result<Self, Self::Error> {
        if let Value::Table(val) = value {
            Ok(val)
        } else {
            Err(value)
        }
    }
}

impl TryFrom<Value> for FunctionObject {
    type Error = Value;

    fn try_from(value: Value) -> std::result::Result<Self, Self::Error> {
        if let Value::Function(val) = value {
            Ok(val)
        } else {
            Err(value)
        }
    }
}
