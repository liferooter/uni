// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::path::PathBuf;

use futures::future::try_join_all;
use futures::{try_join, FutureExt, TryFutureExt};

use super::LazyValue;
use super::{
    object::BuiltInFunction, target::ensure_target, value::ToValue, FunctionObject, ListObject,
    RuntimeError, StringObject, TableObject, Value, ValueObject,
};

macro_rules! builtin_function {
    ($closure:expr) => {
        &|args: Vec<LazyValue>| Box::pin(($closure)(args).map_ok(|val| val.to_value()))
    };
}

macro_rules! assert_n_args {
    ($n:literal, $args:expr) => {
        if $args.len() != $n {
            return Err(RuntimeError::WrongArgumentsNumber {
                recieves: $n,
                passed: $args.len(),
            });
        }
    };
}

#[allow(unused)]
macro_rules! list {
    () => {
        Vec::<Box<dyn ToValue>>::new()
    };
    ( $first:expr $( , $elem:expr )* $(,)? ) => {{
        let vec: Vec<Box<dyn ToValue>> = vec![
            Box::new($first)
            $(, Box::new($elem))*
        ];
        vec
    }};
}

#[allow(unused)]
macro_rules! table {
    () => {
        std::collections::HashMap::<String, Box<dyn ToValue>>::new()
    };
    (
        $first_key:ident : $first_val:expr
        $( , $key:ident : $val:expr )*
        $(,)?
    ) => {{
        let mut table = std::collections::HashMap::<String, Box<dyn ToValue>>::new();
        table.insert(String::from(stringify!($first_key)), Box::new($first_val));
        $(
            table.insert(String::from(stringify!($key)), Box::new($val));
        )*
        table
    }};
}

pub const STDLIB: &str = include_str!("../stdlib.uni");

thread_local! {
    pub static BUILTIN_FUNCTIONS: &'static [(&'static str, BuiltInFunction)] = &[
        (
            "file",
            builtin_function! {
                |args: Vec<LazyValue>| async move {
                    assert_n_args!(1, args);

                    let path: StringObject = args[0]
                        .get()
                        .await?
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "file path",
                            correct: "string",
                            incorrect: val.type_name()
                        })?;

                    Ok(
                        StringObject::from_filename(path)
                    )
                }
            }
        ),
        (
            "target",
            builtin_function! {
                |args: Vec<LazyValue>| async move {
                    assert_n_args!(1, args);

                    let options: TableObject = args[0]
                        .get()
                        .await?
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "target options",
                            correct: "table",
                            incorrect: val.type_name()
                        })?;

                    let (env, command, commands) = try_join!(
                        options
                            .get("env")
                            .map(Option::transpose),
                        options
                            .get("command")
                            .map(Option::transpose),
                        options
                            .get("commands")
                            .map(Option::transpose)
                    )?;

                    let env: TableObject = env
                        .unwrap_or_else(|| table!().to_value())
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "target environment",
                            correct: "table",
                            incorrect: val.type_name()
                        })?;

                    let commands = commands
                        .or_else(|| Some(list!(command?).to_value()))
                        .ok_or(RuntimeError::NotAllowed("targets without commands to run"))?
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "list of target commands",
                            correct: "list",
                            incorrect: val.type_name()
                        })?;

                    ensure_target(commands, env)
                        .await
                }
            }
        ),
        (
            "map",
            builtin_function! {
                |args: Vec<LazyValue>| async move {
                    assert_n_args!(2, args);

                    let (list, func) = try_join!(
                        args[0].get(),
                        args[1].get()
                    )?;

                    let list: ListObject = list
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "first argument of function `map`",
                            correct: "list",
                            incorrect: val.type_name()
                        })?;

                    let func: FunctionObject = func
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "second argument of function `map`",
                            correct: "function",
                            incorrect: val.type_name()
                        })?;

                    Ok(ListObject::from_values(
                        try_join_all(
                            list
                                .list
                                .into_iter()
                                .map(|val| async {
                                    func.clone().call(
                                        vec![val]
                                    )
                                    .await
                                })
                        )
                        .await?
                    ))
                }
            }
        ),
        (
            "reduce",
            builtin_function! {
                |args: Vec<LazyValue>| async move {
                    assert_n_args!(2, args);

                    let (list, func) = try_join!(
                        args[0].get(),
                        args[1].get()
                    )?;

                    let list: ListObject = list
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "first argument of function `reduce`",
                            correct: "list",
                            incorrect: val.type_name()
                        })?;
                    let func: FunctionObject = func
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "second argument of function `reduce`",
                            correct: "function",
                            incorrect: val.type_name()
                        })?;

                    let values: Vec<Value> = list
                        .values()
                        .await?;
                    let mut values = values.into_iter();
                    let mut last = values.next()
                        .unwrap_or(Value::None);
                    for current in values {
                        last = func.call(vec![
                            LazyValue::from(last),
                            LazyValue::from(current)
                        ]).await?;
                    }

                    Ok(last)
                }
            }
        ),
        (
            "fold",
            builtin_function! {
                |args: Vec<LazyValue>| async move {
                    assert_n_args!(3, args);

                    let (list, func, init) = try_join!(
                        args[0].get(),
                        args[1].get(),
                        args[2].get()
                    )?;

                    let list: ListObject = list
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "first argument of function `fold`",
                            correct: "list",
                            incorrect: val.type_name()
                        })?;
                    let func: FunctionObject = func
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "second argument of function `reduce`",
                            correct: "function",
                            incorrect: val.type_name()
                        })?;

                    let values: Vec<Value> = list
                        .values()
                        .await?;
                    let mut res = init;
                    for elem in values {
                        res = func.call(vec![
                            LazyValue::from(res),
                            LazyValue::from(elem)
                        ]).await?;
                    }

                    Ok(res)
                }
            }
        ),
        (
            "replace",
            builtin_function! {
                |args: Vec<LazyValue>| async move {
                    assert_n_args!(3, args);

                    let (orig, old, new) = try_join!(
                        args[0].get(),
                        args[1].get(),
                        args[2].get()
                    )?;

                    let orig: StringObject = orig
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "first argument of function `reflace`",
                            correct: "string",
                            incorrect: val.type_name()
                        })?;
                    let old: StringObject = old
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "second argument of function `reflace`",
                            correct: "string",
                            incorrect: val.type_name()
                        })?;
                    let new: StringObject = new
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "third argument of function `reflace`",
                            correct: "string",
                            incorrect: val.type_name()
                        })?;

                    let res = orig.value.replace(&old.value, &new.value);
                    let deps = orig.deps
                        .into_iter()
                        .chain(old.deps)
                        .chain(new.deps)
                        .collect();

                    Ok(StringObject {
                        value: res,
                        deps
                    })
                }
            }
        ),
        (
            "typeof",
            builtin_function! {
                |args: Vec<LazyValue>| async move {
                    assert_n_args!(1, args);

                    Ok(
                        args[0]
                            .get()
                            .await?
                            .type_name()
                            .to_string()
                    )
                }
            }
        ),
        (
            "contains",
            builtin_function! {
                |args: Vec<LazyValue>| async move {
                    assert_n_args!(2, args);

                    let (string, value) = try_join!(
                        args[0].get(),
                        args[1].get()
                    )?;

                    match string {
                        Value::List(list) => {
                            let list = list.values().await?;
                            for elem in list {
                                if value.eq(&elem).await? {
                                    return Ok(true)
                                }
                            }

                            Ok(false)
                        },
                        Value::String(string) => {
                            let fragment: StringObject = value
                                .try_into()
                                .map_err(|val: Value| RuntimeError::MustBe {
                                    what: "string fragment",
                                    correct: "string",
                                    incorrect: val.type_name()
                                })?;

                            Ok(string.value.contains(&fragment.value))
                        },
                        _ => Err(RuntimeError::MustBe {
                            what: "first argument of function `contains`",
                            correct: "list or string",
                            incorrect: string.type_name()
                        })
                    }
                }
            }
        ),
        (
            "split",
            builtin_function! {
                |args: Vec<LazyValue>| async move {
                    assert_n_args!(2, args);

                    let (string, delimiter) = try_join!(
                        args[0].get(),
                        args[1].get()
                    )?;

                    let StringObject {
                        value: string,
                        deps: orig_deps
                    } = string
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "value to split",
                            correct: "string",
                            incorrect: val.type_name()
                        })?;
                    let StringObject {
                        value: delimiter,
                        deps: delimiter_deps
                    } = delimiter
                        .try_into()
                        .map_err(|val: Value| RuntimeError::MustBe {
                            what: "value to split",
                            correct: "string",
                            incorrect: val.type_name()
                        })?;

                    let deps: Vec<PathBuf> = orig_deps
                        .into_iter()
                        .chain(delimiter_deps)
                        .collect();

                    Ok(ListObject::from_values(
                        string
                            .split(&delimiter)
                            .map(|s| StringObject {
                                value: s.to_string(),
                                deps: deps.clone()
                            }.to_value())
                            .collect()
                    ))
                }
            }
        )
    ];
}
