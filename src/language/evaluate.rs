// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::collections::HashMap;

use async_recursion::async_recursion;

use crate::ast::{ConstantValue, Expression};

use super::{
    lazy::LazyValue,
    operator::{apply_binary, apply_unary},
    scope::Scope,
    Closure, FunctionObject, ListObject, Result, RuntimeError, StringObject, TableObject, Value,
    ValueObject,
};

impl Expression {
    #[async_recursion(?Send)]
    pub async fn evaluate(&self, scope: &Scope) -> Result<Value> {
        Ok(match self {
            Expression::Literal(constant, position) => match constant {
                ConstantValue::Boolean(val) => Value::Boolean(*val),
                ConstantValue::Integer(val) => Value::Integer(*val),
                ConstantValue::String(val) => StringObject::from_literal(val, scope)
                    .await
                    .map_err(|err| err.with_position(position))?
                    .into_value(),
                ConstantValue::None => Value::None,
            },
            Expression::Variable(name, position) => scope
                .get_or_err(name)
                .await
                .map_err(|err| err.with_position(position))?,
            Expression::List(list) => ListObject::new(scope, list.clone()).into_value(),
            Expression::Table(table) => TableObject::new(scope, table.clone()).into_value(),
            Expression::Function { args, body } => {
                FunctionObject::new(scope, args.clone(), *body.clone()).into_value()
            }
            Expression::FunctionCall {
                function,
                args,
                position,
            } => match function.evaluate(scope).await? {
                Value::Function(function) => {
                    function
                        .call(
                            args.iter()
                                .cloned()
                                .map(|expr| LazyValue::Expression(expr, scope.clone()))
                                .collect(),
                        )
                        .await?
                }
                _ => return Err(RuntimeError::CallNonFunction.with_position(position)),
            },
            Expression::UnaryOp(op, expr, position) => apply_unary(expr.evaluate(scope).await?, op)
                .await
                .map_err(|err| err.with_position(position))?,
            Expression::BinaryOp(left, op, right, position) => {
                let (lhs, rhs) = tokio::try_join!(left.evaluate(scope), right.evaluate(scope))?;
                apply_binary(lhs, rhs, op)
                    .await
                    .map_err(|err| err.with_position(position))?
            }
            Expression::Field(obj, field, position) => match obj.evaluate(scope).await? {
                Value::Table(table) => table.get_or_err(field).await?,
                _ => {
                    return Err(
                        RuntimeError::NotAllowed("getting field from non-table object")
                            .with_position(position),
                    )
                }
            },
            Expression::OptionalField(obj, field, position) => match obj.evaluate(scope).await? {
                Value::Table(table) => table.get(field).await.unwrap_or(Ok(Value::None))?,
                _ => {
                    return Err(
                        RuntimeError::NotAllowed("getting field from non-table object")
                            .with_position(position),
                    )
                }
            },
            Expression::Index(list, index, position) => {
                match tokio::try_join!(list.evaluate(scope), index.evaluate(scope))? {
                    (Value::List(list), Value::Integer(index)) => list.get(index).await,
                    (_, Value::Integer(_)) => {
                        return Err(RuntimeError::NotAllowed("indexing of non-list object"))
                    }
                    (_, _) => {
                        return Err(RuntimeError::NotAllowed("indexing with non-integer index"))
                    }
                }
                .map_err(|err| err.with_position(position))?
            }
            Expression::Conditional {
                condition,
                body,
                else_clause,
            } => {
                if condition.evaluate(scope).await?.to_boolean() {
                    body.evaluate(scope).await?
                } else {
                    else_clause.evaluate(scope).await?
                }
            }
            Expression::LetIn { mappings, body } => {
                body.evaluate(&Scope::new(
                    Some(scope.clone()),
                    HashMap::from_iter(
                        mappings
                            .clone()
                            .into_iter()
                            .map(|(key, expr)| (key, Closure::Inherit(expr, scope.clone()))),
                    ),
                ))
                .await?
            }
        })
    }
}
