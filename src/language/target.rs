// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    collections::HashMap,
    fs::{create_dir_all, remove_dir},
    process::Command,
};

use futures::future::join_all;
use sha2::{Digest, Sha256};

use crate::error;

use super::{
    errors::{Result, RuntimeError},
    object::StringObject,
    value::{Value, ValueObject},
    ListObject, TableObject,
};

pub async fn ensure_target(commands: ListObject, env: TableObject) -> Result<Value> {
    let commands = commands.values().await?;

    let env = join_all(
        env.values()
            .await?
            .into_iter()
            .map(|(key, val)| async move { Ok((key, val.to_string().await?)) }),
    )
    .await
    .into_iter()
    .collect::<Result<Vec<(String, StringObject)>>>()?;

    let mut hasher = Sha256::new();

    for (key, val) in &env {
        hasher.update(key);
        hasher.update(val.hash().await?);
    }

    let env = env.into_iter().map(|(key, val)| (key, val.value));

    let commands: Vec<Vec<StringObject>> = join_all(commands.into_iter().map(|cmdline| async {
        let cmdline = if let Value::List(cmdline) = cmdline {
            join_all(
                cmdline
                    .values()
                    .await?
                    .into_iter()
                    .map(|val| async move { val.to_string().await }),
            )
            .await
            .into_iter()
            .collect::<Result<Vec<StringObject>>>()?
        } else {
            return Err(RuntimeError::MustBe {
                what: "target command",
                correct: "list",
                incorrect: cmdline.type_name(),
            });
        };
        Ok(cmdline)
    }))
    .await
    .into_iter()
    .collect::<Result<Vec<_>>>()?;

    for cmdline in &commands {
        // Use prefix to prevent hash collision
        hasher.update(":cmdline:");
        for arg in cmdline {
            hasher.update(arg.hash().await?);
        }
    }

    let commands = commands
        .into_iter()
        .map(|cmdline| {
            cmdline
                .into_iter()
                .map(|val| val.value)
                .collect::<Vec<String>>()
        })
        .collect();

    let hash = hex::encode(hasher.finalize());

    let mut out_dir = crate::BUILD_DIR
        .get()
        .expect("Failed to get build dir")
        .to_owned();

    out_dir.push(".uni-builddir");
    out_dir.push(hash);

    let built = out_dir.exists();

    if !built {
        create_dir_all(out_dir.clone())
            .unwrap_or_else(|err| error!("failed to create target directory: {}", err));
    }

    let out_path = out_dir
        .canonicalize()
        .unwrap()
        .to_string_lossy()
        .to_string();

    if !built {
        if let Err(error) = build_target(commands, &out_path, env.into_iter().collect()) {
            return if let Err(error) = remove_dir(out_dir) {
                Err(RuntimeError::TargetFailed(Some(error)))
            } else {
                Err(error)
            };
        }
    }

    Ok(Value::String(StringObject::from_value(&out_path)))
}

fn build_target(
    commands: Vec<Vec<String>>,
    out_path: &str,
    env: HashMap<String, String>,
) -> Result<()> {
    for cmdline in commands {
        let cmdline: Vec<String> = cmdline
            .into_iter()
            .map(|arg| arg.replace('\0', out_path))
            .collect();

        println!(
            "+ {}",
            cmdline
                .iter()
                .map(|arg| if arg.contains(' ') || arg.contains('\'') {
                    format!("'{}'", StringObject::escape(arg))
                } else {
                    arg.clone()
                })
                .collect::<Vec<_>>()
                .join(" ")
        );

        match Command::new(&cmdline[0])
            .args(&cmdline[1..])
            .envs(&env)
            .current_dir(crate::BUILD_DIR.get().unwrap())
            .spawn()
        {
            Err(error) => return Err(RuntimeError::TargetFailed(Some(error))),
            Ok(mut child) => match child.wait() {
                Err(error) => return Err(RuntimeError::TargetFailed(Some(error))),
                Ok(status) => {
                    if !status.success() {
                        return Err(RuntimeError::TargetFailed(None));
                    }
                }
            },
        }
    }

    Ok(())
}
