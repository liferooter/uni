// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::ast::Expression;

use super::{Closure, RuntimeError, Scope, Value};

#[derive(Debug, Clone)]
pub enum LazyValue {
    Value(Value),
    Expression(Expression, Scope),
}

impl From<Value> for LazyValue {
    fn from(val: Value) -> Self {
        Self::Value(val)
    }
}

impl LazyValue {
    pub async fn get(&self) -> Result<Value, RuntimeError> {
        match self {
            Self::Value(val) => Ok(val.clone()),
            Self::Expression(expr, scope) => expr.evaluate(scope).await,
        }
    }
}

pub trait ToScoped {
    fn to_scoped(self, scope: &Scope) -> LazyValue;
}

impl ToScoped for LazyValue {
    fn to_scoped(self, _: &Scope) -> LazyValue {
        self
    }
}

impl ToScoped for Closure {
    fn to_scoped(self, scope: &Scope) -> LazyValue {
        match self {
            Self::Value(lazy) => lazy,
            Self::Inherit(expr, inherited_scope) => {
                LazyValue::Expression(expr, Scope::new(Some(inherited_scope), scope.names.clone()))
            }
        }
    }
}
