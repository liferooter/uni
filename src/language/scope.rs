// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{collections::HashMap, ops::Deref, rc::Rc};

use async_recursion::async_recursion;

use super::{errors::Result, lazy::ToScoped, Closure, RuntimeError, Value};

#[derive(Clone, Debug)]
pub struct Scope(Rc<imp::Scope>);

impl Deref for Scope {
    type Target = imp::Scope;

    fn deref(&self) -> &Self::Target {
        self.0.deref()
    }
}

impl AsRef<imp::Scope> for Scope {
    fn as_ref(&self) -> &imp::Scope {
        self.0.as_ref()
    }
}

impl Scope {
    pub fn new(parent: Option<Self>, names: HashMap<String, Closure>) -> Self {
        Self(Rc::new(imp::Scope { names, parent }))
    }

    pub fn empty() -> Self {
        Self::new(None, HashMap::new())
    }

    #[async_recursion(?Send)]
    pub async fn get(&self, name: &str) -> Option<Result<Value>> {
        if let Some(closure) = self.names.get(name) {
            Some(closure.clone().to_scoped(self).get().await)
        } else {
            self.parent.as_ref()?.get(name).await
        }
    }

    #[async_recursion(?Send)]
    pub async fn get_or_err(&self, name: &str) -> Result<Value> {
        self.get(name)
            .await
            .unwrap_or_else(|| Err(RuntimeError::NameNotFound(name.to_string())))
    }
}

mod imp {
    use super::Scope as ScopeRef;
    use crate::language::Closure;
    use std::collections::HashMap;

    #[derive(Clone, Default, Debug)]
    pub struct Scope {
        pub names: HashMap<String, Closure>,
        pub parent: Option<ScopeRef>,
    }
}
