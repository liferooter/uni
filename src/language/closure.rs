// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use super::{lazy::LazyValue, scope::Scope};

use crate::ast::Expression;

#[derive(Clone, Debug)]
pub enum Closure {
    Value(LazyValue),
    Inherit(Expression, Scope),
}

impl From<LazyValue> for Closure {
    fn from(lazy: LazyValue) -> Self {
        Self::Value(lazy)
    }
}
