// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::cmp::Ordering;

use super::errors::{Result, RuntimeError};
use super::value::{Value, ValueObject};
use crate::ast::{BinaryOp, UnaryOp};

pub async fn apply_unary(value: Value, op: &UnaryOp) -> Result<Value> {
    match op {
        UnaryOp::Not => Ok(Value::Boolean(!value.to_boolean())),
        UnaryOp::Minus => Ok(Value::Integer(match value {
            Value::Integer(integer) => -integer,
            _ => {
                return Err(RuntimeError::CantUseUnop {
                    op: "-",
                    correct_type: "integer",
                    wrong_type: value.type_name(),
                })
            }
        })),
        UnaryOp::Length => Ok(Value::Integer(match value {
            Value::List(list) => list.len() as i64,
            _ => {
                return Err(RuntimeError::CantUseUnop {
                    op: "#",
                    correct_type: "list",
                    wrong_type: value.type_name(),
                })
            }
        })),
    }
}

pub async fn apply_binary(left: Value, right: Value, op: &BinaryOp) -> Result<Value> {
    Ok(match op {
        BinaryOp::And => Value::Boolean(left.to_boolean() && right.to_boolean()),
        BinaryOp::Or => {
            if left.to_boolean() {
                left
            } else {
                right
            }
        }

        BinaryOp::Power => match (left, right) {
            (Value::Integer(left), Value::Integer(right)) => {
                if right >= 0 {
                    Value::Integer(left.pow(right as u32))
                } else {
                    return Err(RuntimeError::NotAllowed("negative power"));
                }
            }
            (left, right) => {
                return Err(RuntimeError::CantUseBinop {
                    op: "^",
                    wrong_types: (left.type_name(), right.type_name()),
                    correct_types: vec![("integer", "integer")],
                })
            }
        },
        BinaryOp::Multiple => match (left, right) {
            (Value::Integer(left), Value::Integer(right)) => Value::Integer(left * right),
            (left, right) => {
                return Err(RuntimeError::CantUseBinop {
                    op: "*",
                    wrong_types: (left.type_name(), right.type_name()),
                    correct_types: vec![("integer", "integer")],
                })
            }
        },
        BinaryOp::Divide => match (left, right) {
            (Value::Integer(left), Value::Integer(right)) => {
                if right != 0 {
                    Value::Integer(left / right)
                } else {
                    return Err(RuntimeError::NotAllowed("division by zero"));
                }
            }
            (left, right) => {
                return Err(RuntimeError::CantUseBinop {
                    op: "/",
                    wrong_types: (left.type_name(), right.type_name()),
                    correct_types: vec![("integer", "integer")],
                })
            }
        },
        BinaryOp::Mod => match (left, right) {
            (Value::Integer(left), Value::Integer(right)) => {
                if right != 0 {
                    Value::Integer(left % right)
                } else {
                    return Err(RuntimeError::NotAllowed("modulo by zero"));
                }
            }
            (left, right) => {
                return Err(RuntimeError::CantUseBinop {
                    op: "%",
                    wrong_types: (left.type_name(), right.type_name()),
                    correct_types: vec![("integer", "integer")],
                })
            }
        },
        BinaryOp::Plus => match (left, right) {
            (Value::Integer(left), Value::Integer(right)) => Value::Integer(left + right),
            (Value::List(left), Value::List(right)) => Value::List(left + right),
            (Value::String(left), Value::String(right)) => Value::String(left.concat(right)),
            (left, right) => {
                return Err(RuntimeError::CantUseBinop {
                    op: "+",
                    wrong_types: (left.type_name(), right.type_name()),
                    correct_types: vec![("integer", "integer"), ("list", "list")],
                })
            }
        },
        BinaryOp::Minus => match (left, right) {
            (Value::Integer(left), Value::Integer(right)) => Value::Integer(left - right),
            (left, right) => {
                return Err(RuntimeError::CantUseBinop {
                    op: "-",
                    wrong_types: (left.type_name(), right.type_name()),
                    correct_types: vec![("integer", "integer")],
                })
            }
        },
        BinaryOp::Overlay => match (left, right) {
            (Value::Table(left_table), Value::Table(right_table)) => {
                Value::Table(left_table.update(right_table))
            }
            (left, right) => {
                return Err(RuntimeError::CantUseBinop {
                    op: "++",
                    wrong_types: (left.type_name(), right.type_name()),
                    correct_types: vec![("table", "table")],
                })
            }
        },

        BinaryOp::Eq => Value::Boolean(left.cmp(&right).await? == Some(Ordering::Equal)),
        BinaryOp::NotEq => Value::Boolean(left.cmp(&right).await? != Some(Ordering::Equal)),
        BinaryOp::Lt => Value::Boolean(left.cmp(&right).await? == Some(Ordering::Less)),
        BinaryOp::Gt => Value::Boolean(left.cmp(&right).await? == Some(Ordering::Greater)),
        BinaryOp::LtEq => {
            let ordering = left.cmp(&right).await?;
            Value::Boolean(ordering == Some(Ordering::Less) || ordering == Some(Ordering::Equal))
        }
        BinaryOp::GtEq => {
            let ordering = left.cmp(&right).await?;
            Value::Boolean(ordering == Some(Ordering::Greater) || ordering == Some(Ordering::Equal))
        }
    })
}
