// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{io, path::PathBuf};

use termion::{
    color::{Fg, Red, Reset},
    style::{Bold, Reset as ResetStyle},
};

use crate::{
    errors::{CompileError, PrintableMessage},
    runtime::Runtime,
    utils::Position,
};

pub type Result<T> = std::result::Result<T, RuntimeError>;

#[derive(Debug)]
pub enum RuntimeError {
    Located(Box<RuntimeError>, Position),

    NameNotFound(String),
    TargetNotFound(String),
    NotAllowed(&'static str),
    FieldNotFound(String),
    TargetFailed(Option<io::Error>),
    InstallationFailed(io::Error),
    FileError(PathBuf, io::Error),
    Cant(&'static str, &'static str),
    WrongArgumentsNumber {
        recieves: usize,
        passed: usize,
    },
    MustBe {
        what: &'static str,
        correct: &'static str,
        incorrect: &'static str,
    },
    CantUseUnop {
        op: &'static str,
        wrong_type: &'static str,
        correct_type: &'static str,
    },
    CantUseBinop {
        op: &'static str,
        wrong_types: (&'static str, &'static str),
        correct_types: Vec<(&'static str, &'static str)>,
    },
    CallNonFunction,
    IndexOutOfRange,
}

impl RuntimeError {
    /// Convert error to located with given position.
    ///
    /// If error is already located, just pass it forward.
    pub fn with_position(self, position: &Position) -> Self {
        if let Self::Located(..) = self {
            self
        } else {
            Self::Located(Box::new(self), position.clone())
        }
    }

    /// Get one-line error message.
    fn message(&self) -> String {
        match self {
            Self::Located(err, _) => err.message(),

            Self::NameNotFound(name) => format!("name \"{}\" not found", name),
            Self::TargetNotFound(name) => format!("target \"{}\" not found", name),
            Self::NotAllowed(message) => format!("{} is not allowed", message),
            Self::FieldNotFound(field) => format!("field \"{}\" is not found", field),
            Self::FileError(file, error) => format!(
                "error while reading file `{}`: {}",
                file.to_string_lossy(),
                error
            ),
            Self::Cant(what, with_type) => format!("can't {} value of type `{}`", what, with_type),
            Self::TargetFailed(error) => format!(
                "target executing is failed: {}",
                error
                    .as_ref()
                    .map(|err| err.to_string())
                    .unwrap_or_else(|| "proccess failed".to_owned())
            ),
            Self::InstallationFailed(error) => format!("installation failed: {}", error),
            Self::MustBe {
                what,
                correct,
                incorrect,
            } => format!("{} must be {}, not {}", what, correct, incorrect),
            Self::CantUseUnop {
                op,
                wrong_type,
                correct_type,
            } => format!(
                "unary operator `{}` only allowed with {}, not {}",
                op, correct_type, wrong_type
            ),
            Self::CantUseBinop {
                op,
                wrong_types,
                correct_types,
            } => format!(
                "binary operator `{}` only allowed with {}, not {} and {}",
                op,
                correct_types
                    .iter()
                    .map(|(left, right)| format!("{} and {}", left, right))
                    .collect::<Vec<_>>()
                    .join(" or "),
                wrong_types.0,
                wrong_types.1
            ),
            Self::WrongArgumentsNumber { recieves, passed } => format!(
                "wrong number of arguments: function recieves {} arguments, {} passed",
                recieves, passed
            ),
            Self::CallNonFunction => "can't call non-function object".to_string(),
            Self::IndexOutOfRange => "index is out of the range".to_string(),
        }
    }
}

impl PrintableMessage for RuntimeError {
    fn print(&self, runtime: &Runtime) {
        if let Self::Located(err, pos) = self {
            CompileError::new(&err.message(), pos).print(runtime)
        } else {
            eprintln!(
                "{}{}error:{}{} {}",
                Fg(Red),
                Bold,
                ResetStyle,
                Fg(Reset),
                self.message()
            )
        }
    }
}
