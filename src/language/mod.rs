// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

pub use closure::Closure;
pub use errors::{Result, RuntimeError};
pub use lazy::LazyValue;
pub use object::*;
pub use scope::Scope;
pub use value::{ToValue, Value, ValueObject};

mod closure;
mod errors;
mod evaluate;
mod lazy;
mod object;
mod operator;
mod scope;
pub mod stdlib;
mod target;
mod utils;
mod value;
