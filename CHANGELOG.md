## [1.0.2](https://gitlab.com/liferooter/uni/compare/v1.0.1...v1.0.2) (2022-08-29)


### Bug Fixes

* make any tables unequal, like functions ([d595ee7](https://gitlab.com/liferooter/uni/commit/d595ee7fa0b8e8e7dc03b43fb7b58be5bf61dc52))


### Performance Improvements

* pass scope as borrowed reference where can ([871ed2e](https://gitlab.com/liferooter/uni/commit/871ed2ed5d4c591b5020161501eea89dedacf16e))

## [1.0.1](https://gitlab.com/liferooter/uni/compare/v1.0.0...v1.0.1) (2022-08-14)


### Bug Fixes

* **errors:** fix locating errors in stdlib ([a12b735](https://gitlab.com/liferooter/uni/commit/a12b73530e1ee277dd4a3327ec5b48236c65787b))

<!--
SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# 1.0.0 (2022-08-12)

Initial release
