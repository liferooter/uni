<!--
SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# Uni Build System (WIP)

Experimental next-generation pure functional build system inspired by [sleirsgoevy](https://gitlab.com/sleirsgoevy)'s [fbuild](https://gitlab.com/sleirsgoevy/fbuild).

> Still in heavy development. Not ready to use.
